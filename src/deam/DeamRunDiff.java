package deam;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.regex.Matcher;

import dua.Extension;
import dua.Forensics;
import dua.global.ProgramFlowGraph;
import dua.method.CFG;
import dua.method.CFG.CFGNode;
import fault.StmtMapper;

import soot.SootMethod;

/**
 * A component for running subjects and comparing their execution histories
 * 
 * A coordinate class that can both run the subjects and compare execution
 * histories. This component will take two programs as input and run all the
 * test cases specified in the input file, and then differentiate the two
 * execution histories in method-level.
 * 
 * @author TianyuXu
 * 
 */
public class DeamRunDiff implements Extension {

	/**
	 * The input file that containing all the test cases' arguments
	 */
	// This is for Windows:
	// private static final String INPUT_RELATIVE_PATH =
	// "\\inputs\\testinputs.txt";
	// --------------------------------------------------
	// This is for Linux:
	private static final String INPUT_RELATIVE_PATH = "/inputs/testinputs.txt";

	/**
	 * The required argument number of DeamRunDiff
	 */
	private static final int ARG_NUM = 5;

	/**
	 * Arguments that saved for DeamRunDiff. Will not forward to DUAForensics
	 */
	private static String[] savedArgs = new String[ARG_NUM];

	/**
	 * A mapping form impacted method name to impacted statement IDs
	 */
	private static HashMap<String, HashSet<Integer>> methodToStmts = new HashMap<String, HashSet<Integer>>();

	/**
	 * main entrance of running and differentiating component
	 * 
	 * @param args
	 *            the arguments passed by console
	 */
	public static void main(String args[]) {
		// test if there's enough parameters
		if (args.length < ARG_NUM) {
			System.err.println("Not enough params!");
			System.exit(0);
		}

		// save all the parameters for DeamRunDiff
		args = saveArgs(args);

		DeamRunDiff deamRunDiff = new DeamRunDiff();
		Forensics.registerExtension(deamRunDiff);
		Forensics.main(args);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see dua.Extension#run()
	 */
	@Override
	public void run() {
		System.out.println("Running DeamRunDiff extension of DUA-Forensics");
		StmtMapper.getCreateInverseMap();

		// run two programs and get result
		Collection<String> result = startRunAndDiff(savedArgs[0], savedArgs[1],
				savedArgs[2], savedArgs[3], savedArgs[4]);
		System.out.println("Impact set size: " + result.size());

		// print out method name and statement IDs
		for (String methodName : result) {
			System.out
					.println(methodName + " " + methodToStmts.get(methodName));
		}
	}

	/**
	 * Start to run a subject program according to the given parameters, and
	 * compare there execution histories to get the ground truth of impact set
	 * 
	 * @param mainClass
	 *            the name of the entry class used in the instrumentation
	 * @param rootDir
	 *            the root directory of the subject program
	 * @param binDir1
	 *            the directory in which the entry class file lies (version 1)
	 * @param binDir2
	 *            the directory in which the entry class file lies (version 2)
	 * @param stmtId
	 *            change location in the form statement ID
	 * @return the ground truth of impact set
	 */
	public static Collection<String> startRunAndDiff(String mainClass,
			String rootDir, String binDir1, String binDir2, String stmtId) {
		BufferedReader reader;
		// test counter
		int testCnt = 0;
		// streams for execution history output
		final PrintStream stdOut = System.out;
		final PrintStream stdErr = System.err;
		ByteArrayOutputStream outStream1 = new ByteArrayOutputStream(8192);
		ByteArrayOutputStream outStream2 = new ByteArrayOutputStream(8192);
		PrintStream tmpOut1 = new PrintStream(outStream1);
		PrintStream tmpOut2 = new PrintStream(outStream2);
		PrintStream nullStream = new PrintStream(new NullOutStream());
		// result ground truth
		HashSet<String> result = new HashSet<String>();

		try {
			reader = new BufferedReader(new FileReader(new File(rootDir
					+ INPUT_RELATIVE_PATH)));
			String testArgs = reader.readLine();

			// read the test file and run two programs
			while (testArgs != null) {
				testCnt++;
				String[] args = preProcessArgs(testArgs, rootDir);
				stdOut.print(String.format("[Test %d] %s %s\t", testCnt,
						mainClass, testArgs));

				// run program version 1
				System.setOut(tmpOut1);
				URL[] urls1 = { new File(binDir1).toURI().toURL() };
				ClassLoader cl1 = new URLClassLoader(urls1, Thread
						.currentThread().getContextClassLoader());
				Thread.currentThread().setContextClassLoader(cl1);
				Class<?> cls1 = cl1.loadClass(mainClass);
				Method me1 = cls1.getMethod("main", args.getClass());
				stdOut.print("Start Ver 1\t");
				System.setErr(nullStream);
				me1.invoke(null, (Object) args);
				System.setErr(stdErr);
				System.out.flush();

				// run program version 2
				System.setOut(tmpOut2);
				URL[] urls2 = { new File(binDir2).toURI().toURL() };
				ClassLoader cl2 = new URLClassLoader(urls2,
						ClassLoader.getSystemClassLoader());
				Class<?> cls2 = cl2.loadClass(mainClass);
				Method me2 = cls2.getMethod("main", args.getClass());
				stdOut.println("Start Ver 2");
				System.setErr(nullStream);
				me2.invoke(null, (Object) args);
				System.setErr(stdErr);
				System.out.flush();

				// differentiate the execution histories
				Collection<String> newResult = diffExecutionHistory(
						outStream1.toByteArray(), outStream2.toByteArray(),
						stmtId);
				// calculate the union of new and old results
				result.addAll(newResult);

				// clear the output streams
				outStream1.reset();
				outStream2.reset();

				testArgs = reader.readLine();
			}

			System.setOut(stdOut);
			outStream1 = null;
			outStream2 = null;

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}

		return result;
	}

	/**
	 * Give two execution histories, differentiate them and compute the impact
	 * set in method-level
	 * 
	 * @param buf1
	 *            buffer of execution history 1
	 * @param buf2
	 *            buffer of execution history 1
	 * @param stmtID
	 *            the change location of the subject
	 * @return impacted methods set
	 */
	private static Collection<String> diffExecutionHistory(byte[] buf1,
			byte[] buf2, String stmtID) {
		ArrayList<String> result = new ArrayList<String>();

		Hashtable<Integer, String> exeHist1 = new Hashtable<Integer, String>();
		Hashtable<Integer, String> exeHist2 = new Hashtable<Integer, String>();

		// the RegExp indicating the start point of comparing
		String changeLocRegExp = String.format(
				"^\\|\\|\\|\\d+\\[[01]{1}\\]=.*", stmtID);
		String histRegExp = "^\\|\\|\\|\\d+\\[[01]{1}\\]=.*";
		String histPrefix = "|||";
		BufferedReader reader = null;
		String line = null;

		try {
			// whether the changeLoc is found
			boolean changeLocFound = false;
			reader = new BufferedReader(new InputStreamReader(
					new ByteArrayInputStream(buf1)));
			line = reader.readLine();

			while (line != null) {
				// if change location not found yet
				if (!changeLocFound) {
					// if found now
					if (line.matches(changeLocRegExp)) {
						changeLocFound = true;
					}
					// if still not found
					else {
						line = reader.readLine();
						continue;
					}
				}
				// if the line is not a execution history record
				if (!line.matches(histRegExp)) {
					System.err.println("[WARNING] " + line);
					line = reader.readLine();
					continue;
				}

				// extract stmtID - value pairs
				int stmtIDStart = histPrefix.length();
				int stmtIDEnd = line.indexOf('[');
				int valueStart = line.indexOf('=') + 1;
				int currStmtID = Integer.parseInt(line.substring(stmtIDStart,
						stmtIDEnd));
				String value = line.substring(valueStart);
				// remove the address of the object
				// [CAUTION] this approach may cause a problem
				// consider a better way to deal with Objects
				int addrStart = value.lastIndexOf('@');
				if (addrStart != -1) {
					value = value.substring(0, addrStart);
				}

				// add the new history to the mapping
				if (exeHist1.containsKey(currStmtID))
					value = exeHist1.get(currStmtID) + " " + value;
				exeHist1.put(currStmtID, value);

				line = reader.readLine();
			}

			// same as above
			changeLocFound = false;
			reader = new BufferedReader(new InputStreamReader(
					new ByteArrayInputStream(buf2)));
			line = reader.readLine();

			while (line != null) {
				if (!changeLocFound) {
					if (line.matches(changeLocRegExp)) {
						changeLocFound = true;
					} else {
						line = reader.readLine();
						continue;
					}
				}
				if (!line.matches(histRegExp)) {
					System.err.println("[WARNING] " + line);
					line = reader.readLine();
					continue;
				}

				int stmtIDStart = histPrefix.length();
				int stmtIDEnd = line.indexOf('[');
				int valueStart = line.indexOf('=') + 1;
				int currStmtID = Integer.parseInt(line.substring(stmtIDStart,
						stmtIDEnd));
				String value = line.substring(valueStart);
				int addrStart = value.lastIndexOf('@');
				if (addrStart != -1) {
					value = value.substring(0, addrStart);
				}

				if (exeHist2.containsKey(currStmtID))
					value = exeHist2.get(currStmtID) + " " + value;
				exeHist2.put(currStmtID, value);

				line = reader.readLine();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		// compare two execution history mappings
		// get the union of two sets
		HashSet<Integer> ids = new HashSet<Integer>(exeHist1.keySet());
		ids.addAll(exeHist2.keySet());

		for (Integer id : ids) {
			// if there is some different
			if (exeHist1.get(id) == null
					|| !exeHist1.get(id).equals(exeHist2.get(id))) {
				// get the method name
				CFGNode n = StmtMapper.getNodeFromGlobalId(id);
				CFG g = ProgramFlowGraph.inst().getContainingCFG(n);
				SootMethod m = g.getMethod();
				String methodName = m.getDeclaringClass() + "::" + m.getName();
				result.add(methodName);

				// add method - stmtID mapping
				if (methodToStmts.get(methodName) == null) {
					methodToStmts.put(methodName, new HashSet<Integer>());
				}
				methodToStmts.get(methodName).add(id);
			}
		}

		return result;
	}

	/**
	 * Filter and save the arguments for DeamRunDiff, and return other arguments
	 * 
	 * @param args
	 *            all the arguments from console
	 * @return filtered arguments for DUAForensics
	 */
	private static String[] saveArgs(String[] args) {
		List<String> argsFiltered = new ArrayList<String>();

		for (int i = 0; i < ARG_NUM; i++) {
			savedArgs[i] = args[i];
		}

		for (int i = ARG_NUM; i < args.length; i++) {
			argsFiltered.add(args[i]);
		}

		String[] arrArgsFilt = new String[argsFiltered.size()];
		return argsFiltered.toArray(arrArgsFilt);
	}

	/**
	 * Pre-process the arguments, including trimming, splitting the arguments
	 * and handling the relative path
	 * 
	 * @param args
	 *            the arguments passed by console
	 * @param rootDir
	 *            the root directory of the subject program
	 * @return processed arguments
	 */
	private static String[] preProcessArgs(String args, String rootDir) {
		// for Linux:
		args = args.replaceAll("\\\\+", "/").replaceAll("\\s+", " ");
		String[] result = args.trim().split(" ");

		// parse the relative path into absolute path
		for (int i = 0; i < result.length; i++) {
			if (result[i].startsWith("..")) {
				result[i] = result[i].replaceFirst("..",
						Matcher.quoteReplacement(rootDir));
			}
		}
		return result;
	}

}

/**
 * A OutputStream for dropping the outputs, similar to /dev/null in Linux
 * 
 * @author TianyuXu
 * 
 */
final class NullOutStream extends OutputStream {
	@Override
	public void write(int b) throws IOException {
	}
}