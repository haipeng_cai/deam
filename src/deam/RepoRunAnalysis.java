/**
 * File: src/deam/RepoRunAnalysis.java
 * -------------------------------------------------------------------------------------------
 * Date			Author      Changes
 * -------------------------------------------------------------------------------------------
 * 02/05/14		hcai		created; given two revisions from repository of a subject, running 
 *							both EAS/EH instrumented programs, then analysing EAS and mDEA results
 *							on impacted methods, and finally calculating precision, recall and accuracy
 * 02/13/14		hcai		added optional statement mapping loading instead of always computing to speed up 
 * 							the overall differencing process
 * 04/28/14		hcai		improved for subjects that produce large EH outputs
*/
package deam;

import java.io.*;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import profile.ExecHistReporter;

public class RepoRunAnalysis{
	public final static float TRACELEN_THRESHOLD = 0.5f;
	
	private static int MaxRunTimes = 1;
	
	// the necessary location arguments that give the running subjects */
	/** the subject location and version-seed suffix, with which the class paths, inputs and output dirs are to 
	 * be reduced according to the naming routines 
	 */
	protected static String SubjectDir = "";
	protected static String RevBase = "";
	protected static String RevNext = "";
	/** the entry class name */
	protected static String entryClsName = "";
	/** the query method that encloses the active SID and that is used for querying EAS */
	protected static List<String> queryMethods; // = "";
	/** the file giving the EAS full trace length per test - we will these length to determine if a mutation crashed a test */
	protected static String fnTraceLengths = "";
	/** the files giving stmtids.out of the two versions */
	protected static String StmtIdFileBase = "";
	protected static String StmtIdFileNext = "";
		
	// the following parameters are to be reduced from the above according to the naming routines */
	/** the class path for the RepoEA instrumented subject */
	protected static String RepoEABinPath="";
	/** the output path for the RepoEA instrumented subject */
	protected static String RepoEAOutputPath="";
	/** the class path for the RepoEH instrumented subject - base version */
	protected static String RepoEHBinPathBase="";
	/** the output path for the RepoEA instrumented subject - base version */
	protected static String RepoEHOutputPathBase="";
	/** the class path for the RepoEH instrumented subject - changed version */
	protected static String RepoEHBinPathNext="";
	/** the output path for the RepoEA instrumented subject - changed version */
	protected static String RepoEHOutputPathNext="";
	
	/** the input text that indexes all test cases */
	protected static String testInputFile ="";
	/** the number of tests to execute */
	protected static int	nTests = 0;
	
	/** the statement mapping file */
	protected static String fnStmtidMapping ="";
	
	/** keep the default output stream */
	private final static PrintStream stdout = System.out;
	private final static PrintStream stderr = System.err;
	
	/** maintain a list of uncovered tests to save run times */
	protected static Set<Integer> uncoveredTests = new LinkedHashSet<Integer>();
	
	/** map from test to corresponding expected full EAS trace length */
	protected static Map<String, Integer> test2tracelen = new LinkedHashMap<String, Integer>();
	
	/** total crashing tests */
	protected static Integer totalCrashingTests = 0;
	
	/** total number of skipped tests found not covering the query method according EAS full trace */
	protected static Integer totalSkippedTests = 0;
	
	/** in order to avoid crashing the disk space, remove outputs after they have been utilized for IS computations */
	protected static boolean keepOutputs = false;
	
	/** to avoid getting stuck on some test execution because of certain crazy mutations, 
	 * we limit the maximal length of time for each, in minutes */
	protected static long MAXTESTDURATION = 30L;
	
	/** if include the runtime phase in this process or not */
	protected static boolean needRun = false;
	
	/** the map from category to impact set: 
	 * 0: EAS-all 
	 * 1: EAS-crashing 
	 * 2: EAS-non-crashing 
	 * 3: mDEA-all 
	 * 4: mDEA-crashing 
	 * 5: mDEA-non-crashing 
	 */
	protected static Map<Integer, Set<String>> finalResult = new LinkedHashMap<Integer, Set<String>>();
	
	public static void parseArgs(String args[]){
		assert args.length >= 7;
		SubjectDir = args[0];
		RevBase = args[1];
		RevNext = args[2];
		entryClsName = args[3];
		queryMethods = dua.util.Util.parseStringList(args[4], ';');
		fnTraceLengths = args[5];
		nTests = Integer.valueOf(args[6]);
		
		System.err.println("Subject: " + SubjectDir + " base version=" + RevBase + " changed version=" + RevNext +
				" boostrap class=" + entryClsName + " query methods=" + queryMethods + 
				" number of tests=" + nTests);

		RepoEABinPath = SubjectDir + File.separator + RevBase + File.separator + "EAInstrumented";
		RepoEAOutputPath = SubjectDir + File.separator + RevBase + File.separator + "EAoutdyn";
		RepoEHBinPathBase = SubjectDir + File.separator + RevBase + File.separator + "EHInstrumented";
		RepoEHOutputPathBase = SubjectDir + File.separator + RevBase + File.separator + "EHoutdyn";
		RepoEHBinPathNext = SubjectDir + File.separator + RevNext + File.separator + "EHInstrumented";
		RepoEHOutputPathNext = SubjectDir + File.separator + RevNext + File.separator + "EHoutdyn";
		
		StmtIdFileBase = RepoEHBinPathBase + File.separator + "stmtids.out";
		StmtIdFileNext = RepoEHBinPathNext + File.separator + "stmtids.out";
		
		testInputFile = SubjectDir + File.separator + "testinputs.txt";
		
		if (args.length>=8) {
			if ( !(needRun = args[7].equalsIgnoreCase("-needRun")) ) {
				long MaxTestDuration = Long.valueOf(args[7]);
				MAXTESTDURATION = MaxTestDuration;
			}
		}
		
		if (args.length>=9) {
			fnStmtidMapping = args[8].trim(); 
		}
		if (args.length>=10) {
			keepOutputs = args[9].equalsIgnoreCase("-keepOutputs");
		}
		if (args.length>=11) {
			MaxRunTimes = Integer.valueOf(args[10]); 
		}
	}
	
	public static void readTraceLengths() {
		BufferedReader br;
	
		try {
			br = new BufferedReader(new InputStreamReader(new FileInputStream(fnTraceLengths)));
			String ts = br.readLine();
			while(ts != null){
				String[] segs = ts.split("\\s+",2);
				assert segs.length >=2;
				test2tracelen.put(segs[0], Integer.valueOf(segs[1]));
				
				ts = br.readLine();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		if (test2tracelen.keySet().size() != nTests) {
			System.setErr(stderr);
			System.err.println("WARNING: the given test case number [" + nTests +
					"] does NOT match the number of tests from the trace length file " + test2tracelen.keySet().size());
		}
	}
	
	public static void main(String args[]){
		if (args.length < 5) {
			System.err.println("too few arguments.");
			return;
		}
		parseArgs(args);
		
		readTraceLengths();
		
		if (fnStmtidMapping.length()<1) {
			// try locating the mapping data probably dumped before at a protocol place
			fnStmtidMapping =  SubjectDir + File.separator + RevBase + "-" + RevNext + ".stmtmap";
		}
		AlignStmt aligner = new AlignStmt();
		Object oaligner = aligner.DeserializeFromFile(fnStmtidMapping);
		if ( null != oaligner ) {
			System.out.println("Loading statement mapping between revisions " + RevBase + " and " + RevNext);
			deam.DeamAnalysis.setAligner((AlignStmt)oaligner);
		}
		else {
			// read stmtIDs files of the two versions
			System.out.println("Aligning statements between revisions " + RevBase + " and " + RevNext);
			if (0 != deam.DeamAnalysis.setAligner(StmtIdFileBase, StmtIdFileNext)) {
				System.err.println("Error occurred during statement aligning.");
			}
			else {
				// dump stmt mapping to save the computation time for it for the next 
				deam.DeamAnalysis.getAligner().SerializeToFile(fnStmtidMapping);
			}
		}
		
		/** we would run multiple times, each time with a set of different mutations */
		for(int i=1;i<=MaxRunTimes;i++){
			System.out.println("====== current at the Run No.  " + i + "======");
			try {
				totalSkippedTests = 0;
				totalCrashingTests = 0;
				
				startRunSubject(i);
			}
			catch (Throwable t) {
				//System.setErr(stderr);
				System.err.println("ERROR occurred during the runtime phase!");
				t.printStackTrace(stderr);
				/** once fatal errors occurred, won't continue the next run */
			}
			finally {
				//System.setErr(stderr);
				//System.setOut(stdout);
				System.out.println("Effectively " + (nTests - uncoveredTests.size()) + 
						" Tests have exercised the instrumented programs");
			}
		}
	}
		
	public static void startRunSubject(int runIdx) {
		String RepoEAoutputDir = RepoEAOutputPath + File.separator + "/run"+runIdx + File.separator;
		String RepoEHoutputDirBase = RepoEHOutputPathBase + File.separator + "/run"+runIdx + File.separator;
		String RepoEHoutputDirNext = RepoEHOutputPathNext + File.separator + "/run"+runIdx + File.separator;
		
		/** we might just need run multiple times from external command line */
		if (MaxRunTimes == 1) {
			RepoEAoutputDir = RepoEAOutputPath + File.separator;
			RepoEHoutputDirBase = RepoEHOutputPathBase + File.separator;
			RepoEHoutputDirNext = RepoEHOutputPathNext + File.separator;
		}
		
		File dirEAF = new File(RepoEAoutputDir);
		if(!dirEAF.isDirectory())	dirEAF.mkdirs();
		File dirEHFBase = new File(RepoEHoutputDirBase);
		if(!dirEHFBase.isDirectory())	dirEHFBase.mkdirs();
		File dirEHFNext = new File(RepoEHoutputDirNext);
		if(!dirEHFNext.isDirectory())	dirEHFNext.mkdirs();
		
		int n = 0;
		BufferedReader br;
		
		// initialize the final result map
		for (Integer cat = 0; cat < 6; cat++) {
			finalResult.put(cat, new LinkedHashSet<String>());
		}
		
		List<String> Chglist = new ArrayList<String>();
		//Chglist.add(queryMethod);
		Chglist.addAll(queryMethods);
	
		try {
			br = new BufferedReader(new InputStreamReader(new FileInputStream(testInputFile)));
			String ts = br.readLine();
			while(ts != null && n < nTests){
				n++;
				if (uncoveredTests.contains(n)) {
					/** this test was not covered in the previous run, won't be able to get covered this time */
					ts = br.readLine();
					continue;
				}
				final String []args = preProcessArg(ts,SubjectDir);
				
				//System.setOut(stdout);
				//System.out.println("test:  " + args[0] + " " + args[1]);
				
				//////////////////////////////// Run RepoEA instrumented subject //////////////////////////////////////////////
				System.setErr(stderr);
				System.err.println("For RepoEAS current at the test No.  " + n);
				
				final String outputEAF = RepoEAoutputDir + "test" + n + ".out";
				final String errEAF = RepoEAoutputDir + "test" + n + ".err";
				
				// redirect stdout and stderr 
				final File outputFileEA = new File(outputEAF);
				final PrintStream outEA = new PrintStream(new FileOutputStream(outputFileEA)); 
				System.setOut(outEA); 
				final File errFileEA = new File(errEAF);
				final PrintStream errEA = new PrintStream(new FileOutputStream(errFileEA)); 
				System.setErr(errEA);
				
				// set the name of file as the serialization target of method event maps (F followed by L)
				EAS.Monitor.setEventMapSerializeFile(RepoEAoutputDir  + "test"+n+ ".em");
				
				final File runSubEA = new File(RepoEABinPath);
				@SuppressWarnings("deprecation")
				final URL urlEA = runSubEA.toURL();
			    final URL[] urlsEA = new URL[]{urlEA};
			    
			    ExecutorService EAservice = Executors.newSingleThreadExecutor();
			    try {
			        Runnable EARunner = new Runnable() {
				@Override public void run() {
				
				try {
				    ClassLoader clEA = new URLClassLoader( urlsEA, Thread.currentThread().getContextClassLoader() );
				    Thread.currentThread().setContextClassLoader(clEA);
				    
				    Class<?> clsEA = clEA.loadClass(entryClsName);
				    
				    Method meEA=clsEA.getMethod("main", new Class[]{args.getClass()});

				    meEA.invoke(null, new Object[]{(Object)args});
				}
				catch (Exception e) {
					e.printStackTrace();
				}
				
				outEA.flush();
				outEA.close();
				errEA.flush();
				errEA.close();
				
				}};
				Future<?>  EAfuture = EAservice.submit(EARunner);
				EAfuture.get(MAXTESTDURATION*60, TimeUnit.SECONDS);
			    }
			    catch (final InterruptedException e) {
			        // The thread was interrupted during sleep, wait or join
			    	System.setErr(stderr);
					System.err.println("Running RepoEAS at the test No.  " + n + " thread interrupted.");
					EAS.Monitor.terminate("Enforced by EARun.");
					ts = br.readLine();
					continue;
			    }
			    catch (final TimeoutException e) {
			        // Took too long!
			    	System.setErr(stderr);
					System.err.println("Running RepoEAS at the test No.  " + n + " TimeOut after " + MAXTESTDURATION*60 + " seconds");
					EAS.Monitor.terminate("Enforced by EARun.");
					ts = br.readLine();
					continue;
			    }
			    catch (final ExecutionException e) {
			        // An exception from within the Runnable task
			    	System.setErr(stderr);
					System.err.println("Running RepoEAS at the test No.  " + n + " exception thrown during test execution " + e);
					EAS.Monitor.terminate("Enforced by EARun.");
					//System.exit(0);
					ts = br.readLine();
					continue;
			    }
			    finally {
			        EAservice.shutdown();
			        outEA.flush();
					outEA.close();
					errEA.flush();
					errEA.close();
			    }
			    
			    // invoke the "program termination event" for the subject in case there is uncaught exception occurred
			    EAS.Monitor.terminate("Enforced by EARun.");
				
				/** determine whether the current test is a crashing one simply according to the full EAS trace length, compared to the
				 * expected one (obtained from the fixed version)
				 */
				assert test2tracelen.get("test"+n)!=null;
				boolean isCrashing = (EAS.Monitor.getFullTraceLength()*1.0 / test2tracelen.get("test"+n) <= TRACELEN_THRESHOLD);
				
				//////////////////////////////// Compute EAS impact set //////////////////////////////////////////////
				System.setErr(stderr);
				System.err.println("Computing EAS impact set ......  ");
				Set<String> EAIS = new LinkedHashSet<String>();
				int szEAIS = EAS.EAAnalysis.parseSingleTrace(RepoEAoutputDir, n, Chglist, EAIS);
				if (!keepOutputs) {
					deleteFile(outputEAF);
					deleteFile(errEAF);
				}
				if (szEAIS<1) {
					// this test did not cover the query method, no point to run with RepoEH because the mutation point is within the method
					System.err.println("Non-covering test skipped ......  ");
					totalSkippedTests ++;
					ts = br.readLine();
					continue;
				}
				
				//////////////////////////////// Run RepoEHBase instrumented subject //////////////////////////////////////////////
				System.setErr(stderr);
				System.err.println("For RepoEHBase current at the test No.  " + n);
				
				final String outputEHFBase = RepoEHoutputDirBase + "" + n + ".out";
				final String errEHFBase = RepoEHoutputDirBase + "" + n + ".err";
				
				// redirect stdout and stderr 
				final File outputFileEHBase = new File(outputEHFBase);
				final PrintStream outEHBase = new PrintStream(new FileOutputStream(outputFileEHBase)); 
				//System.setOut(outEHBase); 
				ExecHistReporter.outStream = outEHBase;
				final File errFileEHBase = new File(errEHFBase);
				final PrintStream errEHBase = new PrintStream(new FileOutputStream(errFileEHBase)); 
				System.setErr(errEHBase);
				
				final File runSubEHBase = new File(RepoEHBinPathBase);
				@SuppressWarnings("deprecation")
				final URL urlEHBase = runSubEHBase.toURL();        
				final URL[] urlsEHBase = new URL[]{urlEHBase};
				
			    ExecutorService EHserviceBase = Executors.newSingleThreadExecutor();
			    try {
			        Runnable EHRunnerBase = new Runnable() {
					@Override public void run() {
									
					try {
						ClassLoader clEHBase = new URLClassLoader( urlsEHBase, Thread.currentThread().getContextClassLoader() );
						Thread.currentThread().setContextClassLoader(clEHBase);
						    
					    Class<?> clsEHBase = clEHBase.loadClass(entryClsName);
					    
					    Method meEHBase=clsEHBase.getMethod("main", new Class[]{args.getClass()});
	
					    meEHBase.invoke(null, new Object[]{(Object)args});
					}
					catch (Exception e) {
						e.printStackTrace();
					}
					/*
					outEHBase.flush();
					outEHBase.close();
					errEHBase.flush();
					errEHBase.close();
					*/
					}};
					Future<?>  EHfutureBase = EHserviceBase.submit(EHRunnerBase);
					EHfutureBase.get(MAXTESTDURATION*60*2, TimeUnit.SECONDS); // mDEA is expected to take longer to run than EAS
			    }
			    catch (final InterruptedException e) {
			        // The thread was interrupted during sleep, wait or join
			    	System.setErr(stderr);
					System.err.println("Running RepoEHBase at the test No.  " + n + " thread interrupted.");
					if (!keepOutputs) {
						deleteFile(outputEHFBase);
						deleteFile(errEHFBase);
					}

					ts = br.readLine();
					EHserviceBase.shutdown();
					continue;
			    }
			    catch (final TimeoutException e) {
			        // Took too long!
			    	System.setErr(stderr);
					System.err.println("Running RepoEHBase at the test No.  " + n + " TimeOut after " + MAXTESTDURATION*60*2 + " seconds");
					if (!keepOutputs) {
						deleteFile(outputEHFBase);
						deleteFile(errEHFBase);
					}

					ts = br.readLine();
					EHserviceBase.shutdown();
					continue;
			    }
			    catch (final ExecutionException e) {
			        // An exception from within the Runnable task
			    	System.setErr(stderr);
					System.err.println("Running RepoEHBase at the test No.  " + n + " exception thrown during test execution " + e);
					if (!keepOutputs) {
						deleteFile(outputEHFBase);
						deleteFile(errEHFBase);
					}
					ts = br.readLine();
					EHserviceBase.shutdown();
					continue;
			    }
			    finally {
			        EHserviceBase.shutdown();
			        outEHBase.flush();
					outEHBase.close();
					errEHBase.flush();
					errEHBase.close();
			    }
			    
			    ////////////////////////////////Run RepoEHNext instrumented subject //////////////////////////////////////////////
				System.setErr(stderr);
				System.err.println("For RepoEHNext current at the test No.  " + n);
				
				final String outputEHFNext = RepoEHoutputDirNext + "" + n + ".out";
				final String errEHFNext = RepoEHoutputDirNext + "" + n + ".err";
				
				// redirect stdout and stderr 
				final File outputFileEHNext = new File(outputEHFNext);
				final PrintStream outEHNext = new PrintStream(new FileOutputStream(outputFileEHNext)); 
				//System.setOut(outEHNext); 
				ExecHistReporter.outStream = outEHNext;
				final File errFileEHNext = new File(errEHFNext);
				final PrintStream errEHNext = new PrintStream(new FileOutputStream(errFileEHNext)); 
				System.setErr(errEHNext);
				
				final File runSubEHNext = new File(RepoEHBinPathNext);
				@SuppressWarnings("deprecation")
				final URL urlEHNext = runSubEHNext.toURL();        
				final URL[] urlsEHNext = new URL[]{urlEHNext};
				
			    ExecutorService EHserviceNext = Executors.newSingleThreadExecutor();
			    try {
			        Runnable EHRunner = new Runnable() {
					@Override public void run() {
									
					try {
						ClassLoader clEHNext = new URLClassLoader( urlsEHNext, Thread.currentThread().getContextClassLoader() );
						Thread.currentThread().setContextClassLoader(clEHNext);
						    
					    Class<?> clsEHNext = clEHNext.loadClass(entryClsName);
					    
					    Method meEHNext=clsEHNext.getMethod("main", new Class[]{args.getClass()});
	
					    meEHNext.invoke(null, new Object[]{(Object)args});
					}
					catch (Exception e) {
						e.printStackTrace();
					}
					
					outEHNext.flush();
					outEHNext.close();
					errEHNext.flush();
					errEHNext.close();
					
					}};
					Future<?>  EHfutureNext = EHserviceNext.submit(EHRunner);
					EHfutureNext.get(MAXTESTDURATION*60*2, TimeUnit.SECONDS); // mDEA is expected to take longer to run than EAS
			    }
			    catch (final InterruptedException e) {
			        // The thread was interrupted during sleep, wait or join
			    	System.setErr(stderr);
					System.err.println("Running RepoEHNext at the test No.  " + n + " thread interrupted.");
					if (!keepOutputs) {
						deleteFile(outputEHFBase);
						deleteFile(errEHFBase);
						deleteFile(outputEHFNext);
						deleteFile(errEHFNext);
					}
					ts = br.readLine();
					EHserviceNext.shutdown();
					continue;
			    }
			    catch (final TimeoutException e) {
			        // Took too long!
			    	System.setErr(stderr);
					System.err.println("Running RepoEHNext at the test No.  " + n + " TimeOut after " + MAXTESTDURATION*60*2 + " seconds");
					if (!keepOutputs) {
						deleteFile(outputEHFBase);
						deleteFile(errEHFBase);
						deleteFile(outputEHFNext);
						deleteFile(errEHFNext);
					}
					ts = br.readLine();
					EHserviceNext.shutdown();
					continue;
			    }
			    catch (final ExecutionException e) {
			        // An exception from within the Runnable task
			    	System.setErr(stderr);
					System.err.println("Running RepoEHNext at the test No.  " + n + " exception thrown during test execution " + e);
					if (!keepOutputs) {
						deleteFile(outputEHFBase);
						deleteFile(errEHFBase);
						deleteFile(outputEHFNext);
						deleteFile(errEHFNext);
					}
					ts = br.readLine();
					EHserviceNext.shutdown();
					continue;
			    }
			    finally {
			        EHserviceNext.shutdown();
			        outEHNext.flush();
					outEHNext.close();
					errEHNext.flush();
					errEHNext.close();
			    }
				
				//////////////////////////////// Compute mDEA impact set //////////////////////////////////////////////
				System.setErr(stderr);
				System.err.println("Computing mDEA impact set ......  ");
				Collection<String> __mDEAIS = deam.DeamAnalysis.diffExecutionHistory(outputFileEHBase, 
						outputFileEHNext, String.valueOf(Integer.MIN_VALUE));
				if (!keepOutputs) {
					deleteFile(outputEHFBase);
					deleteFile(errEHFBase);
					deleteFile(outputEHFNext);
					deleteFile(errEHFNext);
				}
				Set<String> mDEAIS = new LinkedHashSet<String>(__mDEAIS);
								
				//////////////////////////////// merge result  //////////////////////////////////////////////
				finalResult.get(0).addAll(EAIS);
				finalResult.get(3).addAll(mDEAIS);
				
				finalResult.get(isCrashing?1:2).addAll(EAIS);
				finalResult.get(isCrashing?4:5).addAll(mDEAIS);
				
				if (isCrashing) { 	totalCrashingTests ++; }

				// next test case
				ts = br.readLine();
			}
			
			br.close();
		} catch (Exception e) {
			//System.setOut(stdout);
			e.printStackTrace();
			//return;
		}
		finally {
			dumpStatistics(stdout,stderr);
			//ExecHistReporter.outStream = null;
		}
	}
	
	/** dump impact sets of EAS versus mDEA for the specific mutation point in the given query method */ 
	public static void dumpStatistics(PrintStream os, PrintStream or) {
		if (finalResult.get(0).size() < 1) {
			// if this query method is not covered by any of the nTests tests, nothing to be reported
			return;
		}
		
		// dump complete data
		or.println("==== All EAS impact set of " + queryMethods +" ====");
		for (String m : finalResult.get(0)) {
			or.println(m);
		}
		or.println("==== Crashing EAS impact set of " + queryMethods +" ====");
		for (String m : finalResult.get(1)) {
			or.println(m);
		}
		or.println("==== Non-crashing EAS impact set of " + queryMethods +" ====");
		for (String m : finalResult.get(2)) {
			or.println(m);
		}
		
		or.println("==== All mDEA impact set of " + queryMethods +" ====");
		for (String m : finalResult.get(3)) {
			or.println(m);
		}
		or.println("==== Crashing mDEA impact set of " + queryMethods +" ====");
		for (String m : finalResult.get(4)) {
			or.println(m);
		}
		or.println("==== Non-crashing mDEA impact set of " + queryMethods +" ====");
		for (String m : finalResult.get(5)) {
			or.println(m);
		}
		
		// compute FPs and FNs
		Set<String> IntersectionAll = new LinkedHashSet<String>(finalResult.get(0)); 
		IntersectionAll.retainAll(finalResult.get(3));
		Set<String> FPAll = new LinkedHashSet<String>(finalResult.get(0));
		FPAll.removeAll(finalResult.get(3));
		Set<String> FNAll = new LinkedHashSet<String>(finalResult.get(3));
		FNAll.removeAll(finalResult.get(0));
		
		Set<String> IntersectionC = new LinkedHashSet<String>(finalResult.get(1)); 
		IntersectionC.retainAll(finalResult.get(4));
		Set<String> FPC = new LinkedHashSet<String>(finalResult.get(1));
		FPC.removeAll(finalResult.get(4));
		Set<String> FNC = new LinkedHashSet<String>(finalResult.get(4));
		FNC.removeAll(finalResult.get(1));
		
		Set<String> IntersectionNC = new LinkedHashSet<String>(finalResult.get(2)); 
		IntersectionNC.retainAll(finalResult.get(5));
		Set<String> FPNC = new LinkedHashSet<String>(finalResult.get(2));
		FPNC.removeAll(finalResult.get(5));
		Set<String> FNNC = new LinkedHashSet<String>(finalResult.get(5));
		FNNC.removeAll(finalResult.get(2));
		
		or.println("==== All EAS False-positives of " + queryMethods +" ====");
		for (String m : FPAll) {
			or.println(m);
		}
		or.println("==== All EAS False-negatives of " + queryMethods +" ====");
		for (String m : FNAll) {
			or.println(m);
		}
		or.println("==== Crashing EAS False-positives of " + queryMethods +" ====");
		for (String m : FPC) {
			or.println(m);
		}
		or.println("==== Crashing EAS False-negatives of " + queryMethods +" ====");
		for (String m : FNC) {
			or.println(m);
		}
		or.println("==== Non-crashing EAS False-positives of " + queryMethods +" ====");
		for (String m : FPNC) {
			or.println(m);
		}
		or.println("==== Non-crashing EAS False-negatives of " + queryMethods +" ====");
		for (String m : FNNC) {
			or.println(m);
		}
		or.println();
		or.flush();
		
		DecimalFormat df = new DecimalFormat("#.####");
		
		os.println("==== crashing versus non-crashing tests [" + totalCrashingTests +" : " + (nTests - totalCrashingTests) + "] ====");
		os.println("==== valid versus non-covering tests [" + (nTests-totalSkippedTests) +" : " + totalSkippedTests + "] ====");
		// compute statistics and dump
		//String title1 = "change\tquery\tmp\tEA\tmDEA\tFP\tFN\tprecision\trecall\tF1\tF2";
		String title = RevBase + "\t" + queryMethods + "\t";
		
		double pAll = finalResult.get(0).size()<1?1.0:IntersectionAll.size()*1.0 / finalResult.get(0).size();
		double cAll = finalResult.get(3).size()<1?1.0:IntersectionAll.size()*1.0 / finalResult.get(3).size();
		double pC = finalResult.get(1).size()<1?1.0:IntersectionC.size()*1.0 / finalResult.get(1).size();
		double cC = finalResult.get(4).size()<1?1.0:IntersectionC.size()*1.0 / finalResult.get(4).size();
		double pNC = finalResult.get(2).size()<1?1.0:IntersectionNC.size()*1.0 / finalResult.get(2).size();
		double cNC = finalResult.get(5).size()<1?1.0:IntersectionNC.size()*1.0 / finalResult.get(5).size();
		
		os.print("[All] " + title);
		os.print(finalResult.get(0).size() + "\t" + finalResult.get(3).size() + "\t" + FPAll.size() + "\t" +
				FNAll.size() + "\t" + df.format(pAll) + "\t" + df.format(cAll) + "\t" + 
				df.format(FMeasure(pAll, cAll, 1)) + "\t" + df.format(FMeasure(pAll, cAll, 2)) + "\n");
		
		os.print("[C] " + title);
		os.print(finalResult.get(1).size() + "\t" + finalResult.get(4).size() + "\t" + FPC.size() + "\t" +
				FNC.size() + "\t" + df.format(pC) + "\t" + df.format(cC) + "\t" + 
				df.format(FMeasure(pC, cC, 1)) + "\t" + df.format(FMeasure(pC, cC, 2)) + "\n");
		
		os.print("[NC] " + title);
		os.print(finalResult.get(2).size() + "\t" + finalResult.get(5).size() + "\t" + FPNC.size() + "\t" +
				FNNC.size() + "\t" + df.format(pNC) + "\t" + df.format(cNC) + "\t" + 
				df.format(FMeasure(pNC, cNC, 1)) + "\t" + df.format(FMeasure(pNC, cNC, 2)) + "\n");
		os.println();
		os.flush();
	}
	
	public static double FMeasure(double p, double c, int b) {
		return ( (1+b*b)*(p*c)/(p*(b*b)+c) );
	}
	
	public static String[] preProcessArg(String arg,String dir){
		String s1 = arg.replaceAll("\\\\+","/").replaceAll("\\s+", " ");
 
		if(s1.startsWith(" "))
			s1 = s1.substring(1,s1.length());
		String argArray[] =  s1.split(" ");
		for(int i=0;i<argArray.length;i++){
			if(argArray[i].startsWith("..")){
				argArray[i] = argArray[i].replaceFirst("..", dir);
			}
		}		
		return argArray;
	}
	
	public static int deleteFile(String fname) {
		File fObj = new File(fname);
		try {
			if (fObj.exists() && fObj.isFile()) {
				fObj.delete();
			}
		} 
		catch (SecurityException e) { System.err.println("Couldn't delete file due to security reasons: " + fObj + e); return -2;}
		catch (Throwable e) { System.err.println("Couldn't delete file due to unexpected reason: " + fObj + e); return -1;}
		return 0;
	}
	
	public static int countLines(File aFile) throws IOException {
	    LineNumberReader reader = null;
	    try {
	        reader = new LineNumberReader(new FileReader(aFile));
	        while ((reader.readLine()) != null);
	        return reader.getLineNumber();
	    } catch (Exception ex) {
	        return -1;
	    } finally {
	        if(reader != null) 
	            reader.close();
	    }
	}
} // RepoRunAnalysis

/* vim :set ts=4 tw=4 tws=4 */
