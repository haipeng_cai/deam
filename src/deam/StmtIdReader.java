/**
 * File: src/deam/StmtIdReader.java
 * -------------------------------------------------------------------------------------------
 * Date			Author      Changes
 * -------------------------------------------------------------------------------------------
 * 06/25/14		hcai		created; for reading the DuaF-generated stmtids.out
*/
package deam;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A helper utility for parsing a DuaF-produced stmtids.out file : statement id starting from 0
 */
public class StmtIdReader {
	// internal data fields
	private String fileName;
	private List<String> stmtList = new ArrayList<String>();
	private Map<String, List<Integer>> methodToStmts = new HashMap<String, List<Integer>>();
	private Map<Integer, String> stmtToMethod = new HashMap<Integer, String>();
	private Map<String, Integer> stmtToId = new HashMap<String, Integer>();
	
	/** accessors */
	public List<Integer> getStmtsInMethod(String m) { return methodToStmts.get(m); }
	public Integer getStmtId(String s) { return stmtToId.get(s); }
	public String getStmtText(Integer id) { return stmtList.get(id); }
	public String getMethod(Integer id) { return stmtToMethod.get(id); }
	
	public StmtIdReader() {
		this.fileName = "";
	}
	
	public StmtIdReader(String _file) {
		this.fileName = _file;
		parse();
	}
	
	public void setFile(String _fn) {
		this.fileName = _fn;
		this.stmtList.clear();
		this.methodToStmts.clear();
		this.stmtToMethod.clear();
		this.stmtToId.clear();
		
		parse();
	}
	
	private int parse() {
		if (!(this.stmtList.isEmpty() && this.methodToStmts.isEmpty() && this.stmtToMethod.isEmpty() && this.stmtToId.isEmpty())) {
			// already done
			return 0;
		}
		int ret = deam.AlignStmt.readStmtIdsFromFile(this.fileName, stmtList, methodToStmts, stmtToMethod);
		if (ret < 1) {
			return 0;
		}
		
		// fill stmtToId
		for (int i = 0; i < stmtList.size(); ++i) {
			stmtToId.put(stmtList.get(i), i);
		}
		
		return ret;
	}
} // end of StmtIdReader
