package deam;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.List;

import profile.ExecHistInstrumenter;
import dua.Extension;
import dua.Forensics;
import dua.global.dep.DependenceFinder.NodePoint;
import dua.global.dep.DependenceFinder.NodePoint.NodePointComparator;
import dua.util.Util;
import fault.StmtMapper;

/**
 * DEAM instrument component
 * 
 * Based on the DEA part of SensA. Original version by Swhite_zyj July 2012,
 * modified several times by hcai.
 * 
 * @author TianyuXu
 * 
 */
public class DeamInstAllTempTest implements Extension {

	/**
	 * Main entrance of the instrument component
	 * 
	 * @param args
	 *            the arguments passed by console
	 */
	public static void main(String args[]) {
		args = preProcessArgs(args);

		DeamInstAllTempTest deamInst = new DeamInstAllTempTest();
		Forensics.registerExtension(deamInst);
		Forensics.main(args);
	}

	/**
	 * Get the start statement ID and make a list for forwarding arguments
	 * 
	 * @param args
	 *            the arguments passed by console
	 * @return a list for forwarding arguments
	 */
	private static String[] preProcessArgs(String[] args) {
		int argsSize = args.length;
		String[] arrArgsFilt = new String[argsSize + 3];

		for (int i = 0; i < argsSize; i++) {
			arrArgsFilt[i] = args[i];
		}

		// parameters required by Soot
		arrArgsFilt[argsSize] = "-allowphantom";
		arrArgsFilt[argsSize + 1] = "-keeprepbrs";
		arrArgsFilt[argsSize + 2] = "-paramdefuses";

		return arrArgsFilt;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see dua.Extension#run()
	 */
	@Override
	public void run() {
		System.out.println("Running DEAM extension of DUA-Forensics");
		StmtMapper.getCreateInverseMap();

		// instrument subject to output augmented execution histories of sliced
		// statements for dynamic change-impact assessment
		ExecHistInstrumenter instr = new ExecHistInstrumenter();
		List<NodePoint> pntsSorted = instr.instrument(true);

		// just print all slices by statement IDs
		File fOut = new File(Util.getCreateBaseOutPath() + "fwdslice.out");
		// use buffered writer to boost the output
		try {
			PrintWriter writer = new PrintWriter(new BufferedWriter(
					new FileWriter(fOut)));

			Collections.sort(pntsSorted, NodePointComparator.inst);
			for (NodePoint pTgt : pntsSorted) {
				if (pTgt.getRhsPos() == NodePoint.PRE_RHS)
					writer.print("-");
				writer.println(StmtMapper.getGlobalNodeId(pTgt.getN()));
			}

			writer.flush();
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
