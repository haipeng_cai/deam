package deam;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import profile.ExecHistInstrumenter;
import dua.Extension;
import dua.Forensics;
import dua.global.dep.DependenceFinder.NodePoint;
import dua.global.dep.DependenceFinder.NodePoint.NodePointComparator;
import dua.global.dep.DependenceGraph;
import dua.method.CFG.CFGNode;
import dua.util.Util;
import fault.StmtMapper;

/**
 * DEAM instrument component
 * 
 * Based on the DEA part of SensA. Original version by Swhite_zyj July 2012,
 * modified several times by hcai.
 * 
 * @author TianyuXu
 * 
 */
public class DeamInst implements Extension {

	/**
	 * The option tag for start statement ID. For consistency, use the same
	 * option tag as SensA do - "-start:".
	 */
	private final static String START_STMT_ID_OPT = "-start:";

	/**
	 * The start statement ID of the program. For now, assume exactly one start
	 * point is provided
	 */
	private static int startStmtId= Integer.MIN_VALUE; // hcai: use this default value as stmt id for full-scope instrumentation
	
	/** hcai: added the "blacktypes" option to avoid instrument monitoring objects of the specified types */
	private static List<String> blacktypes = new ArrayList<String>();

	/**
	 * Main entrance of the instrument component
	 * 
	 * @param args
	 *            the arguments passed by console
	 */
	public static void main(String args[]) {
		args = preProcessArgs(args);

		DeamInst deamInst = new DeamInst();
		Forensics.registerExtension(deamInst);
		Forensics.main(args);
	}

	/**
	 * Get the start statement ID and make a list for forwarding arguments
	 * 
	 * @param args
	 *            the arguments passed by console
	 * @return a list for forwarding arguments
	 */
	private static String[] preProcessArgs(String[] args) {
		List<String> argsFiltered = new ArrayList<String>();
		
		for (int i = 0; i < args.length; i++) {
			if (args[i].startsWith(START_STMT_ID_OPT)) {
				startStmtId = Integer.parseInt(args[i]
						.substring(START_STMT_ID_OPT.length()));
			} else if (args[i].startsWith("blacktypes")){
				blacktypes = dua.util.Util.parseStringList(args[i+1], ',');
				i++;
			}
			else {
				argsFiltered.add(args[i]);
			}
		}

		// parameters required by Soot
		argsFiltered.add("-allowphantom");
		argsFiltered.add("-keeprepbrs");
		argsFiltered.add("-paramdefuses");

		String[] arrArgsFilt = new String[argsFiltered.size()];
		return argsFiltered.toArray(arrArgsFilt);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see dua.Extension#run()
	 */
	@Override
	public void run() {
		System.out.println("Running DEAM extension of DUA-Forensics");
		StmtMapper.getCreateInverseMap();
		
		// instrument subject to output augmented execution histories of sliced
		// statements for dynamic change-impact assessment
		String[] blacklist = new String[blacktypes.size()];
		blacktypes.toArray(blacklist);
		ExecHistInstrumenter instr = new ExecHistInstrumenter(blacklist);
		
		/** hcai: added the full-scope instrumentation, namely to instrument at every possible point */
		if (Integer.MIN_VALUE == startStmtId) {
			System.out.println("ExecHistory Instrumentation at everywhere...");
			instr.instrument(true);
			return;
		}
		// -

		DependenceGraph depGraph = null;
		CFGNode nStart = StmtMapper.getNodeFromGlobalId(startStmtId);
		depGraph = new DependenceGraph(
				new NodePoint(nStart, NodePoint.POST_RHS), -1);
		System.out.println("Start:  " + nStart);

		Set<NodePoint> pntsSet = depGraph.getPointsInSlice();
		instr.instrument(pntsSet,true);
		// instr.instrument(pntsSet);

		// just print all slices by statement IDs
		List<NodePoint> pntsSorted = new ArrayList<NodePoint>(pntsSet);
		File fOut = new File(Util.getCreateBaseOutPath() + "fwdslice.out");
		// use buffered writer to boost the output
		try {
			PrintWriter writer = new PrintWriter(new BufferedWriter(
					new FileWriter(fOut)));

			Collections.sort(pntsSorted, NodePointComparator.inst);
			for (NodePoint pTgt : pntsSorted) {
				if (pTgt.getRhsPos() == NodePoint.PRE_RHS)
					writer.print("-");
				writer.println(StmtMapper.getGlobalNodeId(pTgt.getN()));
			}

			writer.flush();
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
