/**
 * File: src/deam/AlignStmt.java
 * -------------------------------------------------------------------------------------------
 * Date			Author      Changes
 * -------------------------------------------------------------------------------------------
 * 01/27/14		hcai		created; for the aligning Jimple statements of two unaligned program versions
 * 01/29/14		hcai		added dual mappings to increase usability
 * 01/30/14		hcai		greatly improved performance, enhanced matching precision by ensuring uniqueness of mapping
 * 02/13/14		hcai		added serialization and deserialization to save computations for the client 
 * 06/25/14		hcai		make readStmtIdsFromFile public so as to make AlignStmt a stmtids.out reader
*/
package deam;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * A component for comparing program execution histories
 * Specifically, this class is set to create a mapping of IR statements between two unaligned program versions.
 *
 */
public class AlignStmt implements Serializable {
	public static final float MATCH_THRESHOLD = 0.5f;
	
	private String fileA;
	private String fileB;
	private List<String> stmtsA = new ArrayList<String>();
	private List<String> stmtsB = new ArrayList<String>();
	private Map<String, List<Integer>> methodToStmtsA = new HashMap<String, List<Integer>>();
	private Map<Integer, String> stmtIDToMethodSignatureA = new HashMap<Integer, String>();
	private Map<String, List<Integer>> methodToStmtsB = new HashMap<String, List<Integer>>();
	private Map<Integer, String> stmtIDToMethodSignatureB = new HashMap<Integer, String>();

	private Map<Integer, Integer> A2B = new LinkedHashMap<Integer, Integer>();
	private Map<Integer, Integer> B2A = new LinkedHashMap<Integer, Integer>();
	
	/** these unmatched points are collected for convenient querying in EH differencing process, where points in these lists can be immediately ignored */
	List<Integer> unMatchedA2B = new ArrayList<Integer>();
	List<Integer> unMatchedB2A = new ArrayList<Integer>();
	
	/** accessories */
	public Map<Integer, Integer> getA2BMapping() {return A2B;}
	public Map<Integer, Integer> getB2AMapping() {return B2A;}
	public List<Integer> getUnMatchedA2B() {return unMatchedA2B;}
	public List<Integer> getUnMatchedB2A() {return unMatchedB2A;}
	public String getSignatureByStmtId(int id) {
		if (stmtIDToMethodSignatureA.containsKey(id)) {
			return stmtIDToMethodSignatureA.get(id);
		}
		assert stmtIDToMethodSignatureB.containsKey(id);
		return stmtIDToMethodSignatureB.get(id);
	}
	
	AlignStmt() {
		this.fileA = "";
		this.fileB = "";
	}
	
	AlignStmt(String _fileA, String _fileB) {
		this.fileA = _fileA;
		this.fileB = _fileB;
	}
	
	protected void setFile(int n, String _fn) {
		if (1==n) this.fileA = _fn;
		if (2==n) this.fileB = _fn;
	}
	
	protected static String pickMethodSignature(String jimpleline) {
		int end = jimpleline.indexOf('-', jimpleline.indexOf('>')) - 1;

		if (end <= 0) { 
			System.err.println("[ERROR] StmtIDs file format error at " + jimpleline);
			System.exit(0);
		}
		
		return jimpleline.substring(0, end);
	}
	
	public static int readStmtIdsFromFile(String fn, List<String> stmts, Map<String, List<Integer>> methodToStmts, Map<Integer, String> stmtIDToMethodSignature) {
		File stmtIDsFile = new File(fn);

		if (!stmtIDsFile.isFile()) {
			System.err.println("[ERROR] StmtIDs file does not exist!");
			return -1;
		}

		int lineCounter = 0;
		try {
			BufferedReader reader = new BufferedReader(new FileReader(stmtIDsFile));
			String s = reader.readLine();

			while (s != null) {
				String msig = pickMethodSignature(s);
				stmtIDToMethodSignature.put(lineCounter, msig);
				List<Integer> ids = methodToStmts.get(msig);
				if (null == ids) {
					ids = new ArrayList<Integer>();
				}
				ids.add(lineCounter);
				methodToStmts.put(msig, ids);

				lineCounter++;
				stmts.add(s);
				s = reader.readLine();
			}

			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		 
		return lineCounter;
	}
	
	public int createMapping(boolean debugOut) 
	{
		stmtsA.clear();
		methodToStmtsA.clear();
		stmtIDToMethodSignatureA.clear();
		stmtsB.clear();
		methodToStmtsB.clear();
		stmtIDToMethodSignatureB.clear();
		
		if (readStmtIdsFromFile(fileA, stmtsA, methodToStmtsA, stmtIDToMethodSignatureA) < 0 ||
				readStmtIdsFromFile(fileB, stmtsB, methodToStmtsB, stmtIDToMethodSignatureB) < 0 ) {
			return -1;
		}
		
		int unMatched = MappingAllStmts();
		if (debugOut) {
			System.err.println("Total number of unmatched statements between " + fileA + " and " + fileB + " = " + unMatched);
			System.err.println("Following statements in " + fileA + " are unmatched in " + fileB + ":");
		}
		for (Integer id : A2B.keySet()) {
			if (null == A2B.get(id)) {
				unMatchedA2B.add(id);
				if (debugOut) {
					System.err.println("\t"+id + " - " + stmtsA.get(id));
				}
			}
		}
		if (debugOut) {
			System.err.println("Following statements in " + fileB + " are unmatched in " + fileA + ":");
		}
		for (Integer id : B2A.keySet()) {
			if (null == B2A.get(id)) {
				unMatchedB2A.add(id);
				if (debugOut) {
					System.err.println("\t"+id + " - " + stmtsB.get(id));
				}
			}
		}
		
		if (debugOut) {
			System.err.println("Following statements are matched between the two files but found different: ");
			for (Map.Entry<Integer, Integer> en:A2B.entrySet()) {
				if (en.getValue() == null) continue;
				if (!stmtsA.get(en.getKey()).equals(stmtsB.get(en.getValue()))) {
					System.err.println("\t < " + en.getKey() + " - " + stmtsA.get(en.getKey()));
					System.err.println("\t > " + en.getValue() + " - " + stmtsB.get(en.getValue()));
				}
			}
		}
		
		return 0;
	}
	
	public static void main(String args[]) {
		// test if there are enough parameters
		if (args.length < 2) {
			System.err.println("[ERROR] Not enough parameters!");
			System.exit(0);
		}
		
		String fileA = args[0], fileB = args[1];
		AlignStmt aligner = new AlignStmt(fileA, fileB);

		if (0 != aligner.createMapping(true)) {
			System.err.println("Fatal error, execution aborted.");
		}

		System.err.flush();
		System.out.flush();
	}
	
	protected int MappingAllStmts() {
		int nunMatchedA2B = 0;
		// find correspondence in B for each statement in A
		for (String ma : methodToStmtsA.keySet()) {
			// method in A is missing in B
			if (!methodToStmtsB.containsKey(ma)) {
				for (Integer id : methodToStmtsA.get(ma)) {
					A2B.put(id, null);
				}
				nunMatchedA2B += methodToStmtsA.get(ma).size();
				continue;
			}
			// both share a method
			nunMatchedA2B += MappingStmtsInMethod(stmtsA, stmtsB, methodToStmtsA.get(ma), methodToStmtsB.get(ma), A2B);
		}

		int nunMatchedB2A = 0;
		// find correspondence in A for each statement in B
		for (String mb : methodToStmtsB.keySet()) {
			
			// method in B is missing in A
			if (!methodToStmtsA.containsKey(mb)) {
				for (Integer id : methodToStmtsB.get(mb)) {
					B2A.put(id, null);
				}
				nunMatchedB2A += methodToStmtsB.get(mb).size();
				continue;
			}
			// both share a method
			nunMatchedB2A += MappingStmtsInMethod(stmtsB, stmtsA, methodToStmtsB.get(mb), methodToStmtsA.get(mb), B2A);
		}
		
		return nunMatchedA2B + nunMatchedB2A;
	}
	
	/** create mapping between statements associated with a same method */
	public static int MappingStmtsInMethod(List<String> stmtsA, List<String> stmtsB, List<Integer> listA, List<Integer> listB, Map<Integer, Integer> mapping) {
		int unMatched = 0;
		for (int i = 0; i < listA.size(); i++) {
			int bestMatch = -1;
			int maxLCSLen = 0;
			assert !mapping.containsKey(listA.get(i));
			for (int j = 0; j < listB.size(); j++) {
				// already matched by previous one
				if (mapping.containsValue(listB.get(j))) { continue; }
				int LCSLen = lcs( stmtsA.get(listA.get(i)), stmtsB.get(listB.get(j))).length();
				if (LCSLen > maxLCSLen) {
					maxLCSLen = LCSLen;
					bestMatch = j;
				}
			}
			// no match
			if (-1 == bestMatch || maxLCSLen < stmtsA.get(listA.get(i)).length()*MATCH_THRESHOLD) {
				mapping.put(listA.get(i), null);
				unMatched ++;
			}
			else {
				mapping.put(listA.get(i), listB.get(bestMatch));
			}
		}
	
		return unMatched;
	}
	
	/** DP version can be much more efficient: taken from http://rosettacode.org/wiki/Longest_common_subsequence */
	public static String lcs(String a, String b) {
		StringBuffer sb = new StringBuffer();
		
		/** special filtering */
		if (a.charAt(0)!=b.charAt(0) || Math.abs(a.length()-b.length())>3) return sb.toString();
		if (a.equals(b)) return a;
		
	    int[][] lengths = new int[a.length()+1][b.length()+1];
	 
	    // row 0 and column 0 are initialized to 0 already
	    for (int i = 0; i < a.length(); i++)
	        for (int j = 0; j < b.length(); j++)
	            if (a.charAt(i) == b.charAt(j))
	                lengths[i+1][j+1] = lengths[i][j] + 1;
	            else
	                lengths[i+1][j+1] = Math.max(lengths[i+1][j], lengths[i][j+1]);
	 
	    // read the substring out from the matrix
	    for (int x = a.length(), y = b.length(); x != 0 && y != 0; ) {
	        if (lengths[x][y] == lengths[x-1][y])
	            x--;
	        else if (lengths[x][y] == lengths[x][y-1])
	            y--;
	        else {
	            assert a.charAt(x-1) == b.charAt(y-1);
	            sb.append(a.charAt(x-1));
	            x--;
	            y--;
	        }
	    }
	 
	    return sb.reverse().toString();
	}
	
	/** simple recursive version: taken from <link>http://rosettacode.org/wiki/Longest_common_subsequence</link> */
	public static String lcs_recursive(String a, String b){
	    int aLen = a.length();
	    int bLen = b.length();
	    if(aLen == 0 || bLen == 0){
	        return "";
	    }else if(a.charAt(aLen-1) == b.charAt(bLen-1)){
	        return lcs_recursive(a.substring(0,aLen-1),b.substring(0,bLen-1))
	            + a.charAt(aLen-1);
	    }else{
	        String x = lcs_recursive(a, b.substring(0,bLen-1));
	        String y = lcs_recursive(a.substring(0,aLen-1), b);
	        return (x.length() > y.length()) ? x : y;
	    }
	}
	// ===
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///                                           SERIALIZATION AND DESERIALIZATION
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private static final long serialVersionUID = 0x438200DE;

    /** serialize the results of the intensive computation here so as to speed up clients, such as unaligned EH differencing, which will deserialize the mappings */
	public int SerializeToFile(String sfn) {
		if ( !sfn.isEmpty() ) {
			FileOutputStream fos;
			try {
				fos = new FileOutputStream(sfn);
				ObjectOutputStream oos = new ObjectOutputStream(fos);
				oos.writeObject(this);
				oos.flush();
				oos.close();
				return 0;
			}
			catch (Exception e) {
				e.printStackTrace();
				return -1;
			}
		}
		return -2;
	} // SerializeToFile
	
	public Object DeserializeFromFile(String dfn) {
		FileInputStream fis;
		try {
			fis = new FileInputStream(dfn);
			
			// reconstruct the static VTG from the given disk file 
			ObjectInputStream ois = new ObjectInputStream(fis);
			return ois.readObject();
		}
		catch (FileNotFoundException e) { 
			System.err.println("Failed to locate the source file from which the stmt-mapping is deserialized specified as " + dfn);
		}
		catch (ClassCastException e) {
			System.err.println("Failed to cast the object deserialized to stmt-mapping structure!");
		}
		catch (IOException e) {
			throw new RuntimeException(e); 
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	} // DeserializeFromFile
	
}
