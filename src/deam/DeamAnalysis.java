package deam;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import util.ExecHistReader;

/**
 * A component for comparing subject execution histories
 * 
 * A coordinate class that can compare execution histories. This component will
 * take two programs' execution histories as input and then differentiate them
 * in method-level.
 * 
 * @author TianyuXu
 * 
 */
public class DeamAnalysis {

	/**
	 * The required argument number of DeamAnalysis
	 */
	private static final int ARG_NUM = 4; // hcai: changed from 4 to 3, for full-scope execHist monitoring, where change location is not required
										  // hcai: changed back to 4, for adding the second stmtid file, used for unaligned execution differencing

	/**
	 * A mapping from impacted method signature to impacted statement IDs
	 */
	private static HashMap<String, HashSet<Integer>> methodToStmts = new HashMap<String, HashSet<Integer>>();

	/**
	 * A mapping from stmtID to method signature
	 */
	private static HashMap<Integer, String> stmtIDToMethodSignature = new HashMap<Integer, String>();

	/** hcai added: number of tests actually processed */
	private static int nProcessedTests = 0;
	public static int getNumOfProcessedTests() { return nProcessedTests; }
	
	/**
	 * main entrance of differentiating component
	 * 
	 * @param args
	 *            The arguments passed by console. The four arguments should be
	 *            the absolute path of execution history 1,the absolute path of
	 *            execution history 2, the absolute path of statement IDs file
	 *            (stmtids.out) and the change location statement ID.
	 */
	public static void main(String args[]) {
		// test if there's enough parameters
		if (args.length < ARG_NUM) {
			System.err.println("[ERROR] Not enough parameters!");
			System.exit(0);
		}

		// hcai: adapted to unaligned EH differencing
		if ( args[2].equals(args[3]) ) {
			// read stmtIDs file
			readStmtIDsFile(args[2]);
		}
		else {
			DeamAnalysis.setAligner(args[2], args[3]);
		}

		// differentiate execution histories
		// Collection<String> result = startDiff(args[0], args[1], args[3]); // hcai: changed to the following for full-scope execHist monitoring
		Collection<String> result = startDiff(args[0], args[1], 
				args.length>ARG_NUM?args[4] : String.valueOf(Integer.MIN_VALUE), true, Integer.MAX_VALUE);
		// -
		
		System.out.println("Impact set size: " + result.size());
		// print out method name and statement IDs
		for (String methodName : result) {
			System.out.println(methodName + " " + methodToStmts.get(methodName));
		}

		System.err.flush();
		System.out.flush();
	}

	/**
	 * 
	 * Compare execution histories of two subjects to get the ground truth of
	 * impact set
	 * 
	 * @param execHistPath1            the absolute path of execution histories 1
	 * @param execHistPath2            the absolute path of execution histories 2
	 * @param stmtId            change location statement ID
	 * @return ground truth of impact set
	 */
	// hcai: added the last two parameters for easier reuse elsewhere
	public static Collection<String> startDiff(String execHistPath1, String execHistPath2,		
																			String stmtId, boolean debugOut, int nTests) {
		// result ground truth
		HashSet<String> result = new HashSet<String>();

		File histDir1 = new File(execHistPath1);
		File histDir2 = new File(execHistPath2);

		// check for the execution directory path
		if (!histDir1.isDirectory() || !histDir2.isDirectory()) {
			System.err.println("[ERROR] Exec hist path does not exist.\nCheck the first two params.");
			System.exit(0);
		}

		// test counter
		int testCnt = 1;
		File execHistFile1 = new File(histDir1.getAbsolutePath(), testCnt	+ ".out");
		File execHistFile2 = new File(histDir2.getAbsolutePath(), testCnt	+ ".out");

		while (execHistFile1.exists() && execHistFile2.exists() && testCnt <= nTests) {
			if (debugOut) {
				System.out.println("[INFO] Now running test " + testCnt);
			}

			// differentiate the execution histories
			Collection<String> newResult = diffExecutionHistory(execHistFile1, execHistFile2, stmtId);

			// compute the union of new and old results
			result.addAll(newResult);

			// Uncomment this code block to inspect the uncovered test cases
			if (debugOut) {
				if (newResult.size() == 0) {
					System.out.println("[INFO] test " + testCnt + " is not impacted.");
				} else {
					System.out.println("--------New impact set size: "	+ result.size());
				}
			}

			testCnt++;
			execHistFile1 = new File(histDir1.getAbsolutePath(), testCnt	+ ".out");
			execHistFile2 = new File(histDir2.getAbsolutePath(), testCnt	+ ".out");
		}

		if (debugOut) {
			System.out.println((testCnt - 1)	+ " execution histories are processed.");
		}
		nProcessedTests = testCnt-1;
		if (debugOut) {
			// print out method name and statement IDs
			for (String methodName : result) {
				System.out.println(methodName + " " + methodToStmts.get(methodName));
			}
		}
		return result;
	}

	/**
	 * Give two execution histories, differentiate them and compute the impact
	 * set in method-level
	 * 
	 * @param s1            execution history 1
	 * @param s2            execution history 2
	 * @param stmtID    the change location of the subject
	 * @return impacted methods set
	 */
	public static Collection<String> diffExecutionHistory(File f1, File f2, String stmtID) {
		// hcai: change the Array type to Set type to save memory
		// ArrayList<String> result = new ArrayList<String>();
		Set<String> result = new LinkedHashSet<String>();

		ExecHistReader execHistReader = new ExecHistReader(Integer.parseInt(stmtID));

		Map<Integer, Object> exeHist1 = execHistReader.readExHist(f1.getPath());
		Map<Integer, Object> exeHist2 = execHistReader.readExHist(f2.getPath());

		// compare two execution history mappings
		// get the union of two sets
		HashSet<Integer> ids = new HashSet<Integer>(exeHist1.keySet());
		ids.addAll(exeHist2.keySet());

		for (Integer id : ids) {
			// if there is some different
			/** hcai: extended to support non-aligned statement ids between the two execution histories 
			if (exeHist1.get(id) == null || !exeHist1.get(id).equals(exeHist2.get(id))) {
			*/
			// get the method signature
			String methodSignature = null;
			
			// - hcai starts
			int id1 = id, id2 = id;
			if (DeamAnalysis.autoAlign()) {
				// ignore unmatched statements (it is reasonable to ignore because the unmatching is caused by the source code addition or removal, which have nothing to do with
				// semantic dependence relative to the test suite
				if (DeamAnalysis.getAligner().getUnMatchedA2B().contains(Math.abs(id)) || DeamAnalysis.getAligner().getUnMatchedB2A().contains(Math.abs(id))) {
					continue;
				}
				if (DeamAnalysis.getAligner().getA2BMapping().containsKey(Math.abs(id1))) {
					id2 = DeamAnalysis.getAligner().getA2BMapping().get(Math.abs(id1));
					if (id < 0) id2*=-1;
				}
				//else if (DeamAnalysis.getAligner().getB2AMapping().containsKey(Math.abs(id2))) {
				else {
					// the id in the merged id list from the EH should be found in at least one of the stmt id mappings; otherwise, the EH does not match the provided stmtids files
					assert DeamAnalysis.getAligner().getB2AMapping().containsKey(Math.abs(id2));
					id1 = DeamAnalysis.getAligner().getB2AMapping().get(Math.abs(id2));
					if (id < 0) id1*=-1;
				}
				
				methodSignature = DeamAnalysis.getAligner().getSignatureByStmtId(Math.abs(id));
			}
			else {
				methodSignature = stmtIDToMethodSignature.get(Math.abs(id));
			}
			
			if (exeHist1.get(id1) == null || !exeHist1.get(id1).equals(exeHist2.get(id2))) {
			// --	
				if (methodSignature == null) {
					System.err.println("[ERROR] No method signature found!");
					System.err.println("\tAt stmtid: " + id);
					System.exit(0);
				}

				// add method - stmtID mapping
				if (methodToStmts.get(methodSignature) == null) {
					methodToStmts.put(methodSignature, new HashSet<Integer>());
				}
				methodToStmts.get(methodSignature).add(id);

				// add result
				result.add(methodSignature);
			}
		}

		return result;
	}

	/**
	 * Read stmtIDs file and make the mapping
	 * 
	 * @param path the absolute path of stmtIDs file
	 */
	public static void readStmtIDsFile(String path) {
		File stmtIDsFile = new File(path);

		if (!stmtIDsFile.isFile()) {
			System.err.println("[ERROR] StmtIDs file does not exist!");
			System.exit(0);
		}

		try {
			BufferedReader reader = new BufferedReader(new FileReader(stmtIDsFile));
			String s = reader.readLine();
			int lineCounter = 0;

			while (s != null) {
				int end = s.indexOf('-', s.indexOf('>')) - 1;

				if (end <= 0) {
					System.err.println("[ERROR] StmtIDs file format error!");
					System.exit(0);
				}
				// add mappings
				stmtIDToMethodSignature.put(lineCounter++, s.substring(0, end));

				s = reader.readLine();
			}

			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/** hcai: extended for supporting unaligned EH differencing */
	protected static AlignStmt aligner = null;
	public static boolean autoAlign() { return aligner!=null; }
	public static AlignStmt getAligner() { return aligner; }
	public static int setAligner(String stmtidFileA, String stmtidFileB, boolean debugOut) {
		if (null != aligner) {
			// already set
			return 0;
		}
		aligner = new AlignStmt(stmtidFileA, stmtidFileB);
		int ret = aligner.createMapping(debugOut);
		if (0 != ret) {
			// unsuccessfully aligner setup equals to a fact that no aligner can be used
			aligner = null;
		}
		return ret;
	}
	public static int setAligner(String stmtidFileA, String stmtidFileB) {
		return setAligner(stmtidFileA, stmtidFileB, false);
	}
	public static void setAligner(AlignStmt __aligner) {
		aligner = __aligner;
	}
	public static void resetAligner() { aligner = null; }
}

