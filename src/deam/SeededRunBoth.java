/**
 * File: src/deam/SeededRunBoth.java
 * -------------------------------------------------------------------------------------------
 * Date			Author      Changes
 * -------------------------------------------------------------------------------------------
 * 10/05/13		hcai		created; for running EAS on the buggy version and mDEA on both buggy and fixed version,
 *							and then compute the accuracy of EAS based on the actual impacts given by mDEA
*/
package deam;

import java.io.*;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import profile.ExecHistReporter;

public class SeededRunBoth{
	public final static float TRACELEN_THRESHOLD = 0.5f;
	
	private static int MaxRunTimes = 1;
	
	// the necessary location arguments that give the running subjects */
	/** the subject location and version-seed suffix, with which the class paths, inputs and output dirs are to 
	 * be reduced according to the naming routines 
	 */
	protected static String SubjectDir = "";
	protected static String VersionSeed = "";
	/** the entry class name */
	protected static String entryClsName = "";
	/** the query method that encloses the active SID and that is used for querying EAS */
	protected static String queryMethod = "";
	/** the mutation location to be activated given by a statement id */
	protected static int activeSID = Integer.MIN_VALUE;
	/** the file giving the EAS full trace length per test - we will these length to determine if a mutation crashed a test */
	protected static String fnTraceLengths = "";
	/** the file giving stmtids.out of the ation-instrumented subject */
	protected static String StmtIdFile = "";
		
	// the following parameters are to be reduced from the above according to the naming routines */
	/** the class path for the EA instrumented subject */
	protected static String EABinPath="";
	/** the output path for the EA instrumented subject */
	protected static String EAOutputPath="";
	/** the class path for the EH instrumented subject1 */
	protected static String EHBinPath1="";
	/** the output path for the EA instrumented subject1 */
	protected static String EHOutputPath1="";
	/** the class path for the EH instrumented subject2 */
	protected static String EHBinPath2="";
	/** the output path for the EA instrumented subject2 */
	protected static String EHOutputPath2="";
	
	/** the input text that indexes all test cases */
	protected static String testInputFile ="";
	/** the number of tests to execute */
	protected static int	nTests = 0;
	
	/** keep the default output stream */
	private final static PrintStream stdout = System.out;
	private final static PrintStream stderr = System.err;
	
	/** map from test to corresponding expected full EAS trace length */
	protected static Map<String, Integer> test2tracelen = new LinkedHashMap<String, Integer>();
	
	/** total crashing tests */
	protected static Integer totalCrashingTests = 0;
	
	/** total number of skipped tests found not covering the query method according EAS full trace */
	protected static Integer totalSkippedTests = 0;
	
	/** in order to avoid crashing the disk space, remove outputs after they have been utilized for IS computations */
	protected static boolean keepOutputs = true;
	
	/** to avoid getting stuck on some test execution because of certain crazy mutations, 
	 * we limit the maximal length of time for each, in minutes */
	protected static long MAXTESTDURATION = 30L;
	
	/** the map from category to impact set: 
	 * 0: EAS-all 
	 * 1: EAS-crashing 
	 * 2: EAS-non-crashing 
	 * 3: mDEA-all 
	 * 4: mDEA-crashing 
	 * 5: mDEA-non-crashing 
	 */
	protected static Map<Integer, Set<String>> finalResult = new LinkedHashMap<Integer, Set<String>>();
	
	public static void parseArgs(String args[]){
		assert args.length >= 8;
		SubjectDir = args[0];
		VersionSeed = args[1];
		entryClsName = args[2];
		queryMethod = args[3];
		activeSID = Integer.valueOf(args[4]);
		fnTraceLengths = args[5];
		StmtIdFile = args[6];
		nTests = Integer.valueOf(args[7]);
		System.err.println("Subject: " + SubjectDir + " ver-seed=" + VersionSeed +
				" boostrap class=" + entryClsName + " query method=" + queryMethod + 
				" activeSID=" + activeSID + " number of tests=" + nTests);
		
		EABinPath = SubjectDir + File.separator + "EAInstrumented-" + VersionSeed;
		EAOutputPath = SubjectDir + File.separator + "EAoutdyn-" + VersionSeed;
		EHBinPath1 = SubjectDir + File.separator + "EHInstrumented-" + VersionSeed;
		EHOutputPath1 = SubjectDir + File.separator + "EHoutdyn-" + VersionSeed;
		EHBinPath2 = SubjectDir + File.separator + "EHInstrumented-" + VersionSeed+"-orig";
		EHOutputPath2 = SubjectDir + File.separator + "EHoutdyn-" + VersionSeed+"-orig";
				
		testInputFile = SubjectDir + File.separator + "inputs" + File.separator + "testinputs.txt";
		
		if (args.length>=9) {
			long MaxTestDuration = Long.valueOf(args[8]);
			MAXTESTDURATION = MaxTestDuration;
		}
		
		if (args.length>=10) {
			keepOutputs = args[9].equalsIgnoreCase("-keepOutputs");
		}
		if (args.length>=11) {
			MaxRunTimes = Integer.valueOf(args[10]); 
		}
	}
	
	public static void readTraceLengths() {
		BufferedReader br;
	
		try {
			br = new BufferedReader(new InputStreamReader(new FileInputStream(fnTraceLengths)));
			String ts = br.readLine();
			while(ts != null){
				String[] segs = ts.split("\\s+",2);
				assert segs.length >=2;
				test2tracelen.put(segs[0], Integer.valueOf(segs[1]));
				
				ts = br.readLine();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		if (test2tracelen.keySet().size() != nTests) {
			System.setErr(stderr);
			System.err.println("WARNING: the given test case number [" + nTests +
					"] does NOT match the number of tests from the trace length file " + test2tracelen.keySet().size());
		}
	}
	
	public static void main(String args[]){
		if (args.length < 5) {
			System.err.println("too few arguments.");
			return;
		}
		parseArgs(args);
		
		readTraceLengths();
		
		// read stmtIDs file
		deam.DeamAnalysis.readStmtIDsFile(StmtIdFile);
		
		/** we would run multiple times, each time with a set of different mutations */
		for(int i=1;i<=MaxRunTimes;i++){
			System.out.println("====== current at the Run No.  " + i + "======");
			try {
				totalSkippedTests = 0;
				totalCrashingTests = 0;
				
				startRunSubject(i);
			}
			catch (Throwable t) {
				System.setErr(stderr);
				System.err.println("ERROR occurred during the runtime phase!");
				t.printStackTrace(stderr);
				/** once fatal errors occurred, won't continue the next run */
			}
			finally {
				System.setErr(stderr);
				System.setOut(stdout);
			}
		}
	}
		
	public static void startRunSubject(int runIdx) {
		String EAoutputDir = EAOutputPath + File.separator + "/run"+runIdx + File.separator;
		String EHoutputDir1 = EHOutputPath1 + File.separator + "/run"+runIdx + File.separator;
		String EHoutputDir2 = EHOutputPath2 + File.separator + "/run"+runIdx + File.separator;
		/** we might just need run multiple times from external command line */
		if (MaxRunTimes == 1) {
			EAoutputDir = EAOutputPath + File.separator;
			EHoutputDir1 = EHOutputPath1 + File.separator;
			EHoutputDir2 = EHOutputPath2 + File.separator;
		}
		
		File dirEAF = new File(EAoutputDir);
		if(!dirEAF.isDirectory())	dirEAF.mkdirs();
		File dirEHF1 = new File(EHoutputDir1);
		if(!dirEHF1.isDirectory())	dirEHF1.mkdirs();
		File dirEHF2 = new File(EHoutputDir2);
		if(!dirEHF2.isDirectory())	dirEHF2.mkdirs();
		
		int n = 0;
		BufferedReader br;
		
		// initialize the final result map
		for (Integer cat = 0; cat < 6; cat++) {
			finalResult.put(cat, new LinkedHashSet<String>());
		}
		
		List<String> Chglist = new ArrayList<String>();
		Chglist.add(queryMethod);
	
		try {
			br = new BufferedReader(new InputStreamReader(new FileInputStream(testInputFile)));
			String ts = br.readLine();
			while(ts != null && n < nTests){
				n++;
				final String []args = preProcessArg(ts,SubjectDir);
				
				//System.setOut(stdout);
				//System.out.println("test:  " + args[0] + " " + args[1]);
				
				//////////////////////////////// Run EA instrumented subject //////////////////////////////////////////////
				System.setErr(stderr);
				System.err.println("For EAS current at the test No.  " + n);
				
				final String outputEAF = EAoutputDir + "test" + n + ".out";
				final String errEAF = EAoutputDir + "test" + n + ".err";
				
				// redirect stdout and stderr 
				final File outputFileEA = new File(outputEAF);
				final PrintStream outEA = new PrintStream(new FileOutputStream(outputFileEA)); 
				System.setOut(outEA); 
				final File errFileEA = new File(errEAF);
				final PrintStream errEA = new PrintStream(new FileOutputStream(errFileEA)); 
				System.setErr(errEA);
				
				// set the name of file as the serialization target of method event maps (F followed by L)
				EAS.Monitor.setEventMapSerializeFile(EAoutputDir  + "test"+n+ ".em");
				
				final File runSubEA = new File(EABinPath);
				@SuppressWarnings("deprecation")
				final URL urlEA = runSubEA.toURL();
			    final URL[] urlsEA = new URL[]{urlEA};
			    
			    ExecutorService EAservice = Executors.newSingleThreadExecutor();
			    try {
			        Runnable EARunner = new Runnable() {
				@Override public void run() {
				
				try {
				    ClassLoader clEA = new URLClassLoader( urlsEA, Thread.currentThread().getContextClassLoader() );
				    Thread.currentThread().setContextClassLoader(clEA);
				    
				    Class<?> clsEA = clEA.loadClass(entryClsName);
				    
				    Method meEA=clsEA.getMethod("main", new Class[]{args.getClass()});

				    meEA.invoke(null, new Object[]{(Object)args});
				}
				catch (Exception e) {
					e.printStackTrace();
				}
				
				outEA.flush();
				outEA.close();
				errEA.flush();
				errEA.close();
				
				}};
				Future<?>  EAfuture = EAservice.submit(EARunner);
				EAfuture.get(MAXTESTDURATION*60, TimeUnit.SECONDS);
			    }
			    catch (final InterruptedException e) {
			        // The thread was interrupted during sleep, wait or join
			    	System.setErr(stderr);
					System.err.println("Running EAS at the test No.  " + n + " thread interrupted.");
					EAS.Monitor.terminate("Enforced by EARun.");
					ts = br.readLine();
					continue;
			    }
			    catch (final TimeoutException e) {
			        // Took too long!
			    	System.setErr(stderr);
					System.err.println("Running EAS at the test No.  " + n + " TimeOut after " + MAXTESTDURATION*60 + " seconds");
					EAS.Monitor.terminate("Enforced by EARun.");
					ts = br.readLine();
					continue;
			    }
			    catch (final ExecutionException e) {
			        // An exception from within the Runnable task
			    	System.setErr(stderr);
					System.err.println("Running EAS at the test No.  " + n + " exception thrown during test execution");
					EAS.Monitor.terminate("Enforced by EARun.");
					ts = br.readLine();
					continue;
			    }
			    finally {
			        EAservice.shutdown();
			    }
			    
			    // invoke the "program termination event" for the subject in case there is uncaught exception occurred
			    EAS.Monitor.terminate("Enforced by EARun.");
				
				/** determine whether the current test is a crashing one simply according to the full EAS trace length, compared to the
				 * expected one (obtained from the fixed version)
				 */
				assert test2tracelen.get("test"+n)!=null;
				boolean isCrashing = (EAS.Monitor.getFullTraceLength()*1.0 / test2tracelen.get("test"+n) <= TRACELEN_THRESHOLD);
				
				//////////////////////////////// Compute EAS impact set //////////////////////////////////////////////
				System.setErr(stderr);
				System.err.println("Computing EAS impact set ......  ");
				Set<String> EAIS = new LinkedHashSet<String>();
				int szEAIS = EAS.EAAnalysis.parseSingleTrace(EAoutputDir, n, Chglist, EAIS);
				if (!keepOutputs) {
					deleteFile(outputEAF);
					deleteFile(errEAF);
				}
				if (szEAIS<1) {
					// this test did not cover the query method, no point to run with EH because the mutation point is within the method
					System.err.println("Non-covering test skipped ......  ");
					totalSkippedTests ++;
					ts = br.readLine();
					continue;
				}
				
				//////////////////////////////// Run EH1 instrumented subject //////////////////////////////////////////////
				System.setErr(stderr);
				System.err.println("For EH1 current at the test No.  " + n);
				
				final String outputEHF1 = EHoutputDir1 + "" + n + ".out";
				final String errEHF1 = EHoutputDir1 + "" + n + ".err";
				
				// redirect stdout and stderr 
				final File outputFileEH1 = new File(outputEHF1);
				final PrintStream outEH1 = new PrintStream(new FileOutputStream(outputFileEH1)); 
				System.setOut(outEH1); 
				final File errFileEH1 = new File(errEHF1);
				final PrintStream errEH1 = new PrintStream(new FileOutputStream(errFileEH1)); 
				System.setErr(errEH1);
				
				final File runSubEH1 = new File(EHBinPath1);
				@SuppressWarnings("deprecation")
				final URL urlEH1 = runSubEH1.toURL();        
				final URL[] urlsEH1 = new URL[]{urlEH1};
				
			    ExecHistReporter.outStream = outEH1;
			    
			    ExecutorService EHservice1 = Executors.newSingleThreadExecutor();
			    try {
			        Runnable EHRunner1 = new Runnable() {
				@Override public void run() {
								
				try {
					ClassLoader clEH1 = new URLClassLoader( urlsEH1, Thread.currentThread().getContextClassLoader() );
					Thread.currentThread().setContextClassLoader(clEH1);
					    
				    Class<?> clsEH1 = clEH1.loadClass(entryClsName);
				    
				    Method meEH1=clsEH1.getMethod("main", new Class[]{args.getClass()});

				    meEH1.invoke(null, new Object[]{(Object)args});
				}
				catch (Exception e) {
					e.printStackTrace();
				}
				
				outEH1.flush();
				outEH1.close();
				errEH1.flush();
				errEH1.close();
				
				}};
				Future<?>  EHfuture1 = EHservice1.submit(EHRunner1);
				EHfuture1.get(MAXTESTDURATION*60*10, TimeUnit.SECONDS); // mDEA is expected to take longer to run than EAS
			    }
			    catch (final InterruptedException e) {
			        // The thread was interrupted during sleep, wait or join
			    	System.setErr(stderr);
					System.err.println("Running EH1 at the test No.  " + n + " thread interrupted.");
					ts = br.readLine();
					continue;
			    }
			    catch (final TimeoutException e) {
			        // Took too long!
			    	System.setErr(stderr);
					System.err.println("Running EH1 at the test No.  " + n + " TimeOut after " + MAXTESTDURATION*60*10 + " seconds");
					ts = br.readLine();
					continue;
			    }
			    catch (final ExecutionException e) {
			        // An exception from within the Runnable task
			    	System.setErr(stderr);
					System.err.println("Running EH1 at the test No.  " + n + " exception thrown during test execution");
					ts = br.readLine();
					continue;
			    }
			    finally {
			        EHservice1.shutdown();
			    }
			    
			    //////////////////////////////// Run EH2 instrumented subject //////////////////////////////////////////////
				System.setErr(stderr);
				System.err.println("For EH2 current at the test No.  " + n);
				
				final String outputEHF2 = EHoutputDir2 + "" + n + ".out";
				final String errEHF2 = EHoutputDir2 + "" + n + ".err";
				
				// redirect stdout and stderr 
				final File outputFileEH2 = new File(outputEHF2);
				final PrintStream outEH2 = new PrintStream(new FileOutputStream(outputFileEH2)); 
				System.setOut(outEH2); 
				final File errFileEH2 = new File(errEHF2);
				final PrintStream errEH2 = new PrintStream(new FileOutputStream(errFileEH2)); 
				System.setErr(errEH2);
				
				final File runSubEH2 = new File(EHBinPath2);
				@SuppressWarnings("deprecation")
				final URL urlEH2 = runSubEH2.toURL();        
				final URL[] urlsEH2 = new URL[]{urlEH2};
				
			    ExecHistReporter.outStream = outEH2;
			    
			    ExecutorService EHservice2 = Executors.newSingleThreadExecutor();
			    try {
			        Runnable EHRunner2 = new Runnable() {
				@Override public void run() {
								
				try {
					/*
					ClassLoader clEH2 = new URLClassLoader(urlsEH2, ClassLoader.getSystemClassLoader());
					Class<?> clsEH2 = clEH2.loadClass(entryClsName);
					
					Method meEH2=clsEH2.getMethod("main", new Class[]{args.getClass()});
					meEH2.invoke(null, new Object[]{(Object)args});
					*/
					
					ClassLoader clEH2 = new URLClassLoader( urlsEH2, Thread.currentThread().getContextClassLoader() );
					Thread.currentThread().setContextClassLoader(clEH2);
					    
				    Class<?> clsEH2 = clEH2.loadClass(entryClsName);
				    
				    Method meEH2=clsEH2.getMethod("main", new Class[]{args.getClass()});

				    meEH2.invoke(null, new Object[]{(Object)args});
				}
				catch (Exception e) {
					e.printStackTrace();
				}
				
				outEH2.flush();
				outEH2.close();
				errEH2.flush();
				errEH2.close();
				
				}};
				Future<?>  EHfuture2 = EHservice2.submit(EHRunner2);
				EHfuture2.get(MAXTESTDURATION*60*10, TimeUnit.SECONDS); // mDEA is expected to take longer to run than EAS
			    }
			    catch (final InterruptedException e) {
			        // The thread was interrupted during sleep, wait or join
			    	System.setErr(stderr);
					System.err.println("Running EH2 at the test No.  " + n + " thread interrupted.");
					ts = br.readLine();
					continue;
			    }
			    catch (final TimeoutException e) {
			        // Took too long!
			    	System.setErr(stderr);
					System.err.println("Running EH2 at the test No.  " + n + " TimeOut after " + MAXTESTDURATION*60*10 + " seconds");
					ts = br.readLine();
					continue;
			    }
			    catch (final ExecutionException e) {
			        // An exception from within the Runnable task
			    	System.setErr(stderr);
					System.err.println("Running EH2 at the test No.  " + n + " exception thrown during test execution");
					ts = br.readLine();
					continue;
			    }
			    finally {
			        EHservice2.shutdown();
			    }
				
				//////////////////////////////// Compute mDEA impact set //////////////////////////////////////////////
				System.setErr(stderr);
				System.err.println("Computing mDEA impact set ......  ");
				System.setErr(stderr);
				//File outputFileNonMutEHF = new File(SubjectDir + File.separator + "NonMutEHoutdyn-" +	VersionSeed+"-orig" + File.separator + "" + n + ".out");
				Collection<String> __mDEAIS = deam.DeamAnalysis.diffExecutionHistory(outputFileEH1, 
						outputFileEH2/*outputFileNonMutEHF*/, String.valueOf(Integer.MIN_VALUE));
				if (!keepOutputs) {
					deleteFile(outputEHF1);
					deleteFile(errEHF1);
					//deleteFile(outputEHF2);
					//deleteFile(errEHF2);
				}
				Set<String> mDEAIS = new LinkedHashSet<String>(__mDEAIS);
								
				//////////////////////////////// merge result  //////////////////////////////////////////////
				finalResult.get(0).addAll(EAIS);
				finalResult.get(3).addAll(mDEAIS);
				
				finalResult.get(isCrashing?1:2).addAll(EAIS);
				finalResult.get(isCrashing?4:5).addAll(mDEAIS);
				
				if (isCrashing) { 	totalCrashingTests ++; }

				// next test case
				ts = br.readLine();
			}
			
			br.close();
		} catch (Exception e) {
			System.setOut(stdout);
			e.printStackTrace();
			//return;
		}
		finally {
			dumpStatistics(stdout,stderr);
		}
	}
	
	/** dump impact sets of EAS versus mDEA for the specific mutation point in the given query method */ 
	public static void dumpStatistics(PrintStream os, PrintStream or) {
		if (finalResult.get(0).size() < 1) {
			// if this query method is not covered by any of the nTests tests, nothing to be reported
			return;
		}
		
		// dump complete data
		or.println("==== All EAS impact set of [" + queryMethod +"] at location [" + activeSID + "] ====");
		for (String m : finalResult.get(0)) {
			or.println(m);
		}
		or.println("==== Crashing EAS impact set of [" + queryMethod +"] at location [" + activeSID + "] ====");
		for (String m : finalResult.get(1)) {
			or.println(m);
		}
		or.println("==== Non-crashing EAS impact set of [" + queryMethod +"] at location [" + activeSID + "] ====");
		for (String m : finalResult.get(2)) {
			or.println(m);
		}
		
		or.println("==== All mDEA impact set of [" + queryMethod +"] at location [" + activeSID + "] ====");
		for (String m : finalResult.get(3)) {
			or.println(m);
		}
		or.println("==== Crashing mDEA impact set of [" + queryMethod +"] at location [" + activeSID + "] ====");
		for (String m : finalResult.get(4)) {
			or.println(m);
		}
		or.println("==== Non-crashing mDEA impact set of [" + queryMethod +"] at location [" + activeSID + "] ====");
		for (String m : finalResult.get(5)) {
			or.println(m);
		}
		
		// compute FPs and FNs
		Set<String> IntersectionAll = new LinkedHashSet<String>(finalResult.get(0)); 
		IntersectionAll.retainAll(finalResult.get(3));
		Set<String> FPAll = new LinkedHashSet<String>(finalResult.get(0));
		FPAll.removeAll(finalResult.get(3));
		Set<String> FNAll = new LinkedHashSet<String>(finalResult.get(3));
		FNAll.removeAll(finalResult.get(0));
		
		Set<String> IntersectionC = new LinkedHashSet<String>(finalResult.get(1)); 
		IntersectionC.retainAll(finalResult.get(4));
		Set<String> FPC = new LinkedHashSet<String>(finalResult.get(1));
		FPC.removeAll(finalResult.get(4));
		Set<String> FNC = new LinkedHashSet<String>(finalResult.get(4));
		FNC.removeAll(finalResult.get(1));
		
		Set<String> IntersectionNC = new LinkedHashSet<String>(finalResult.get(2)); 
		IntersectionNC.retainAll(finalResult.get(5));
		Set<String> FPNC = new LinkedHashSet<String>(finalResult.get(2));
		FPNC.removeAll(finalResult.get(5));
		Set<String> FNNC = new LinkedHashSet<String>(finalResult.get(5));
		FNNC.removeAll(finalResult.get(2));
		
		or.println("==== All EAS False-positives of [" + queryMethod +"] at location [" + activeSID + "] ====");
		for (String m : FPAll) {
			or.println(m);
		}
		or.println("==== All EAS False-negatives of [" + queryMethod +"] at location [" + activeSID + "] ====");
		for (String m : FNAll) {
			or.println(m);
		}
		or.println("==== Crashing EAS False-positives of [" + queryMethod +"] at location [" + activeSID + "] ====");
		for (String m : FPC) {
			or.println(m);
		}
		or.println("==== Crashing EAS False-negatives of [" + queryMethod +"] at location [" + activeSID + "] ====");
		for (String m : FNC) {
			or.println(m);
		}
		or.println("==== Non-crashing EAS False-positives of [" + queryMethod +"] at location [" + activeSID + "] ====");
		for (String m : FPNC) {
			or.println(m);
		}
		or.println("==== Non-crashing EAS False-negatives of [" + queryMethod +"] at location [" + activeSID + "] ====");
		for (String m : FNNC) {
			or.println(m);
		}
		or.println();
		or.flush();
		
		DecimalFormat df = new DecimalFormat("#.####");
		
		os.println("==== crashing versus non-crashing tests [" + totalCrashingTests +" : " + (nTests - totalCrashingTests) + "] ====");
		os.println("==== valid versus non-covering tests [" + (nTests-totalSkippedTests) +" : " + totalSkippedTests + "] ====");
		// compute statistics and dump
		//String title1 = "change\tquery\tmp\tEA\tmDEA\tFP\tFN\tprecision\trecall\tF1\tF2";
		String title = VersionSeed + "\t" + queryMethod + "\t" + activeSID + "\t";
		
		double pAll = finalResult.get(0).size()<1?1.0:IntersectionAll.size()*1.0 / finalResult.get(0).size();
		double cAll = finalResult.get(3).size()<1?1.0:IntersectionAll.size()*1.0 / finalResult.get(3).size();
		double pC = finalResult.get(1).size()<1?1.0:IntersectionC.size()*1.0 / finalResult.get(1).size();
		double cC = finalResult.get(4).size()<1?1.0:IntersectionC.size()*1.0 / finalResult.get(4).size();
		double pNC = finalResult.get(2).size()<1?1.0:IntersectionNC.size()*1.0 / finalResult.get(2).size();
		double cNC = finalResult.get(5).size()<1?1.0:IntersectionNC.size()*1.0 / finalResult.get(5).size();
		
		os.print("[All] " + title);
		os.print(finalResult.get(0).size() + "\t" + finalResult.get(3).size() + "\t" + FPAll.size() + "\t" +
				FNAll.size() + "\t" + df.format(pAll) + "\t" + df.format(cAll) + "\t" + 
				df.format(FMeasure(pAll, cAll, 1)) + "\t" + df.format(FMeasure(pAll, cAll, 2)) + "\n");
		
		os.print("[C] " + title);
		os.print(finalResult.get(1).size() + "\t" + finalResult.get(4).size() + "\t" + FPC.size() + "\t" +
				FNC.size() + "\t" + df.format(pC) + "\t" + df.format(cC) + "\t" + 
				df.format(FMeasure(pC, cC, 1)) + "\t" + df.format(FMeasure(pC, cC, 2)) + "\n");
		
		os.print("[NC] " + title);
		os.print(finalResult.get(2).size() + "\t" + finalResult.get(5).size() + "\t" + FPNC.size() + "\t" +
				FNNC.size() + "\t" + df.format(pNC) + "\t" + df.format(cNC) + "\t" + 
				df.format(FMeasure(pNC, cNC, 1)) + "\t" + df.format(FMeasure(pNC, cNC, 2)) + "\n");
		os.println();
		os.flush();
	}
	
	public static double FMeasure(double p, double c, int b) {
		return ( (1+b*b)*(p*c)/(p*(b*b)+c) );
	}
	
	public static String[] preProcessArg(String arg,String dir){
		String s1 = arg.replaceAll("\\\\+","/").replaceAll("\\s+", " ");
 
		if(s1.startsWith(" "))
			s1 = s1.substring(1,s1.length());
		String argArray[] =  s1.split(" ");
		for(int i=0;i<argArray.length;i++){
			if(argArray[i].startsWith("..")){
				argArray[i] = argArray[i].replaceFirst("..", dir);
			}
		}		
		return argArray;
	}
	
	public static int deleteFile(String fname) {
		File fObj = new File(fname);
		try {
			if (fObj.exists() && fObj.isFile()) {
				fObj.delete();
			}
		} 
		catch (SecurityException e) { System.err.println("Couldn't delete file due to security reasons: " + fObj + e); return -2;}
		catch (Throwable e) { System.err.println("Couldn't delete file due to unexpected reason: " + fObj + e); return -1;}
		return 0;
	}
	
	public static int countLines(File aFile) throws IOException {
	    LineNumberReader reader = null;
	    try {
	        reader = new LineNumberReader(new FileReader(aFile));
	        while ((reader.readLine()) != null);
	        return reader.getLineNumber();
	    } catch (Exception ex) {
	        return -1;
	    } finally {
	        if(reader != null) 
	            reader.close();
	    }
	}
} // SeededRunBoth

/* vim :set ts=4 tw=4 tws=4 */
