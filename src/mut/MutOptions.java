/**
 * File: src/mut/MutOptions.java
 * -------------------------------------------------------------------------------------------
 * Date			Author      Changes
 * -------------------------------------------------------------------------------------------
 * 09/24/13		hcai		created; for command-line argument processing for MutEA Instrumenter
 * 01/11/14		hcai		added option of instrumenting all applicable mutation points instead of randomly selecting a subset
 *
*/
package mut;

import java.util.ArrayList;
import java.util.List;
import EAS.*;

public class MutOptions extends EAOptions {
	/** While we are using Random strategy for mutations, we want to use a fixed seed to guarantee reproducibility */
	public static long seed = System.currentTimeMillis();
	public static long getSeed() { return seed; }
	public static long getFixedSeed() { return 1377469450888L; }
	public static void updateSeed() { seed = System.currentTimeMillis(); }
	
	/** if, after inserting mutation probes, just doing EH instrumentation instead of EAS instrumentation */
	protected boolean instrExecHist = false;
	public boolean instrExecHist() { return instrExecHist; }
	/** if, after inserting mutation probes, just doing EAS instrumentation instead of EH instrumentation */
	protected boolean instrEAS = false;
	public boolean instrEAS() { return instrEAS; }
	/** if, instead of selecting a subset of mutation points, just dumping all applicable mutation points */
	protected boolean allApplicablePoints = false;
	public boolean allApplicablePoints() { return allApplicablePoints; }
	
	// when doing EH instrumentation, use the "blacktypes" option to avoid instrument monitoring objects of the specified types
	protected List<String> blacktypes = new ArrayList<String>();
	public List<String> blacktypes() { return blacktypes; }
	
	public final static int OPTION_NUM = EAOptions.OPTION_NUM + 1;
	
	@Override public String[] process(String[] args) {
		args = super.process(args);
		
		List<String> argsFiltered = new ArrayList<String>();
		for (int i = 0; i < args.length; ++i) {
			String arg = args[i];
			
			if (arg.equalsIgnoreCase("-instrExecHist")) {
				instrExecHist = true;
			}
			else if (arg.equalsIgnoreCase("-instrEAS")) {
				instrEAS = true;
			}
			else if (arg.equalsIgnoreCase("-allApplicablePoints")) {
				allApplicablePoints = true;
			}
			else if (arg.equalsIgnoreCase("-blacktypes")) {
				blacktypes = dua.util.Util.parseStringList(args[i+1], ',');
				i++;
			}
			else {
				argsFiltered.add(arg);
			}
		}
		
		String[] arrArgsFilt = new String[argsFiltered.size()];
		return (String[]) argsFiltered.toArray(arrArgsFilt);
	}
}

/* vim :set ts=4 tw=4 tws=4 */

