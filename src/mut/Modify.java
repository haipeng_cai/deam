/**
 * File: src/mut/Modify.java
 * -------------------------------------------------------------------------------------------
 * Date			Author      Changes
 * -------------------------------------------------------------------------------------------
 * 09/24/13		hcai		created; for the Random mutator used by the MutEA Instrumenter as the target probes
 * 09/25/13		hcai		fixed character modification algo
 * 10/01/13		hcai		ensured that the program, once got into a deadly state due to a mutation value, can 
 * 									escape and recover; by using the original value and then stop mutating until it is reset
 * 12/15/13		hcai		added supports for multi-method change -- activating multiple mutation points at a time
*/
package mut;

import java.util.Collection;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class Modify {

	public static void __link() { }
	
	public static float returnFloat =0.0F, preFloat = 0.0F, orgFloat = 0.0F;
	public static int returnInt = 0, preInt = 0, orgInt = 0;
	public static double returnDouble = 0.0D, preDouble = 0.0D, orgDouble = 0.0D;
	public static short returnShort = 0, preShort = 0, orgShort = 0;
	public static byte returnByte, preByte, orgByte;
	public static long returnLong = 0L, preLong = 0L, orgLong = 0L;
	public static char returnChar, preChar, orgChar;
	public static boolean returnBool, preBool, orgBool;
	public static String returnString = "", preString = "", orgString="";
	public static Object returnObject, preObject, orgObject;
	
	private final static Random r = new Random(MutOptions.getSeed());
	private final static int MAXTRIAL = 20000;
	public static int MAXINVOCATION = 20000;
	
	private static Integer callCounter = 0;
	public static void resetCallCounter() { callCounter = 0; stopModification = false; }
	public static boolean hasModifiedAny() { return callCounter > 0; }
	
	/** In each execution, we only *Activate* some of the many mutation points, such that 
	 * we mock a base version containing only a single change 
	 */
	protected static Set<Integer> active_sids = new HashSet<Integer>();// = Integer.MIN_VALUE;
	public static void setActivePoints(Collection<Integer> sids) { active_sids.clear(); active_sids.addAll(sids); }
	public static void setActivePoint(int sid) { active_sids.clear(); active_sids.add(sid); }
	
	public static void setSeed(long seed) {	r.setSeed(seed); }
	
	/** once the program got into a deadly state because of the mutation, we use the "recover"
	 * process to change the value at the mutation point back to the very original one (namely the
	 * first one observed before the first modification); And, stop modification until the call-counter
	 * is reset
	 */
	private static boolean stopModification = false;
	
	public static Number randPickNumber(Number curv, int sid) {
		if (!active_sids.contains(sid) || stopModification) {
		//if (active_sids.contains(sid)sid != active_sid || stopModification) {	
			// if not activated, then return the unchanged value
			return curv;
		}
		
		Number retv = curv.intValue() + r.nextInt(curv.intValue()*2) - curv.intValue();
		int itry = 0;
		// we do want to ensure there is a change!
		while (itry<MAXTRIAL && retv.equals(curv)) {
			retv = curv.intValue() + r.nextInt(curv.intValue()*2) - curv.intValue();
			itry ++;
		}
		return retv;
	}
		
	public synchronized static void modify(String type, int sid) {
		//dua.util.Util.nameToType(type);
		if (!active_sids.contains(sid) || stopModification) {
		//if (sid != active_sid || stopModification) {	
			passOver(type);
			return;
		}
		
		synchronized (callCounter) {
			if (callCounter==0) {
				save(type);
			}
			callCounter ++;
			// For random strategies, from the 1st occurrence (of the modify invocation), all occurrences 
			// afterwards will use the same random value picked up in the 1st one, which makes
			// the change behavior of the mutator closer to realistic occasions
			/** Unlike in SensA, we do want to use different values for each invocation in order to simulate
			 * Not just a fixed value but a function on the program state!
			 */
			/*
			if (callCounter>=2) {
				if (callCounter > MAXINVOCATION) {
					passOver(type);
				}
				return;
			}
			*/
			if (callCounter > MAXINVOCATION) {
				recover(type);
				passOver(type);
				stopModification = true;
				return;
			}
		}
		
		if(type.equals("int")){
			//returnInt = preInt; 
			returnInt =	randPickNumber(preInt, sid).intValue();
		}
		else if(type.equals("float")){
			//returnFloat = preFloat;
			returnFloat =	randPickNumber(preFloat, sid).floatValue();
		}
		else if(type.equals("double")){
			//returnDouble = preDouble;
			returnDouble = randPickNumber(preDouble, sid).doubleValue();
		}
		else if(type.equals("long")){
			//returnLong = preLong;
			returnLong = randPickNumber(preLong, sid).longValue();
		}
		else if(type.equals("byte")){
			//returnByte = preByte;
			returnByte = randPickNumber(preByte, sid).byteValue();
		}
		else if(type.equals("short")){
			//returnShort = preShort;
			returnShort = randPickNumber(preShort, sid).shortValue();
		}
		else if(type.equals("char")){
			//returnChar = preChar;
			//returnChar = randPickNumber(Character.getNumericValue((int)preChar), sid).toString().charAt(0);
			//returnChar = Character.toChars((Integer)randPickNumber((int)preChar, sid))[0];
			returnChar = (char) mut.Modify.randPickNumber((int)preChar, sid).intValue();
		}
		else if(type.equals("boolean")){
			returnBool = preBool;
			//if (sid == active_sid) {
			if (active_sids.contains(sid)) {	
				// we do want to ensure there is a change!
				returnBool = !preBool;
			}
		}
		else if(preObject instanceof String){
			preString = (String)preObject;
			returnString = preString;
			//if (sid == active_sid) {
			if (active_sids.contains(sid)) {
				// use the same Random strategy for String modification as SensA does
				returnString = new Sensa.ModifyRandom().stringModify(preString, 0);
			}
			returnObject = returnString;
		}
		else {
			// we actually did not instrument mutations for any value of Non-primitive and Non-String types
			returnObject = preObject;
		}
		
		//valueChange(type);
	} // modify

	/** simply use all the current values without any mutations */
	public static void passOver(String type) {
		//dua.util.Util.nameToType(type);
		if(type.equals("int")){
			returnInt = preInt; 
		}
		else if(type.equals("float")){
			returnFloat = preFloat;
		}
		else if(type.equals("double")){
			returnDouble = preDouble;
		}
		else if(type.equals("long")){
			returnLong = preLong;
		}
		else if(type.equals("byte")){
			returnByte = preByte;
		}
		else if(type.equals("short")){
			returnShort = preShort;
		}
		else if(type.equals("char")){
			returnChar = preChar;
		}
		else if(type.equals("boolean")){
			returnBool = preBool;
		}
		else if(preObject instanceof String){
			preString = (String)preObject;
			returnString = preString;
			returnObject = returnString;
		}
		else {
			returnObject = preObject;
		}
	} // passOver
	
	/** save the very first original value of the variable to be mutated in order to recover later on*/
	public synchronized static void save(String type) {
		//dua.util.Util.nameToType(type);
		synchronized (callCounter) {
			if (callCounter != 0) {
				// we only save values at the very first time before making any modifications
				return;
			}
		}
		
		if(type.equals("int")){
			orgInt = preInt; 
		}
		else if(type.equals("float")){
			orgFloat = preFloat;
		}
		else if(type.equals("double")){
			orgDouble = preDouble;
		}
		else if(type.equals("long")){
			orgLong = preLong;
		}
		else if(type.equals("byte")){
			orgByte = preByte;
		}
		else if(type.equals("short")){
			orgShort = preShort;
		}
		else if(type.equals("char")){
			orgChar = preChar;
		}
		else if(type.equals("boolean")){
			orgBool = preBool;
		}
		else if(preObject instanceof String){
			preString = (String)preObject;
			orgString = preString;
			orgObject = orgString;
		}
		else {
			orgObject = preObject;
		}
	} // save
	
	/** simply use all the original values without any mutations to let the program escapes  a deadly state*/
	public static void recover(String type) {
		//dua.util.Util.nameToType(type);
		if(type.equals("int")){
			preInt = orgInt; 
		}
		else if(type.equals("float")){
			preFloat = orgFloat;
		}
		else if(type.equals("double")){
			preDouble = orgDouble;
		}
		else if(type.equals("long")){
			preLong = orgLong;
		}
		else if(type.equals("byte")){
			preByte = orgByte;
		}
		else if(type.equals("short")){
			preShort = orgShort;
		}
		else if(type.equals("char")){
			preChar = orgChar;
		}
		else if(type.equals("boolean")){
			preBool = orgBool;
		}
		else if(preObject instanceof String){
			orgString = (String)orgObject;
			preString = orgString;
			preObject = preString;
		}
		else {
			preObject = orgObject;
		}
	} // recover
	
	/** simply verify that EAS and mDEA used the same set of mutations for a same mutation point in the execution of a same test
	 * in a single same run 
	 */
	public static void valueChange(String type) {
		//dua.util.Util.nameToType(type);
		if(type.equals("int")){
			System.out.println("value change from preValue=" + preInt + " to returnValue=" + returnInt);
		}
		else if(type.equals("float")){
			System.out.println("value change from preValue=" + preFloat + " to returnValue=" + returnFloat);
		}
		else if(type.equals("double")){
			System.out.println("value change from preValue=" + preDouble + " to returnValue=" + returnDouble);
		}
		else if(type.equals("long")){
			System.out.println("value change from preValue=" + preLong + " to returnValue=" + returnLong);
		}
		else if(type.equals("byte")){
			System.out.println("value change from preValue=" + preByte + " to returnValue=" + returnByte);
		}
		else if(type.equals("short")){
			System.out.println("value change from preValue=" + preShort + " to returnValue=" + returnShort);
		}
		else if(type.equals("char")){
			System.out.println("value change from preValue=" + preChar + " to returnValue=" + returnChar);
		}
		else if(type.equals("boolean")){
			System.out.println("value change from preValue=" + preBool + " to returnValue=" + returnBool);
		}
		else if(preObject instanceof String){
			System.out.println("value change from preValue=" + preString + " to returnValue=" + returnString);
		}
		else {
			System.out.println("!!!! Really??? We did not mutate any non-string object!!!");
			returnObject = preObject;
		}
	} // passOver
}
