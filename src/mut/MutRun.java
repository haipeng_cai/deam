/**
 * File: src/mut/MutRun.java
 * -------------------------------------------------------------------------------------------
 * Date			Author      Changes
 * -------------------------------------------------------------------------------------------
 * 09/26/13		hcai		created; for the running phase of a MutEAS/MutEH instrumented program 
*/
package mut;

import java.io.*;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.LinkedHashSet;
import java.util.Set;

import profile.ExecHistReporter;

public class MutRun{
	public static int callCounter = 0;
	
	private static int MaxRunTimes = 1;
	
	// the necessary location arguments that give the running subjects */
	/** the subject location and version-seed suffix, with which the class paths, inputs and output dirs are to 
	 * be reduced according to the naming routines 
	 */
	protected static String SubjectDir = "";
	protected static String VersionSeed = "";
	/** the entry class name */
	protected static String entryClsName = "";
	/** the mutation location to be activated given by a statement id */
	protected static int activeSID = Integer.MIN_VALUE;
	
	// the following parameters are to be reduced from the above according to the naming routines */
	/** the class path for the MutEA instrumented subject */
	protected static String MutEABinPath="";
	/** the output path for the MutEA instrumented subject */
	protected static String MutEAOutputPath="";
	/** the class path for the MutEH instrumented subject */
	protected static String MutEHBinPath="";
	/** the output path for the MutEA instrumented subject */
	protected static String MutEHOutputPath="";
	
	/** the input text that indexes all test cases */
	protected static String testInputFile ="";
	/** the number of tests to execute */
	protected static int	nTests = 0;
	
	/** keep the default output stream */
	private final static PrintStream stdout = System.out;
	private final static PrintStream stderr = System.err;
	
	/** maintain a list of uncovered tests to save run times */
	protected static Set<Integer> uncoveredTests = new LinkedHashSet<Integer>();
	
	public static void parseArgs(String args[]){
		assert args.length >= 5;
		SubjectDir = args[0];
		VersionSeed = args[1];
		entryClsName = args[2];
		activeSID = Integer.valueOf(args[3]);
		nTests = Integer.valueOf(args[4]);
		System.out.println("Subject: " + SubjectDir + " ver-seed=" + VersionSeed +
				" boostrap class=" + entryClsName + " activeSID=" + activeSID + " number of tests=" + nTests);
		
		MutEABinPath = SubjectDir + File.separator + "MutEAInstrumented-" + VersionSeed;
		MutEAOutputPath = SubjectDir + File.separator + "MutEAoutdyn-" + VersionSeed;
		MutEHBinPath = SubjectDir + File.separator + "MutEHInstrumented-" + VersionSeed;
		MutEHOutputPath = SubjectDir + File.separator + "MutEHoutdyn-" + VersionSeed;
		
		testInputFile = SubjectDir + File.separator + "inputs" + File.separator + "testinputs.txt";
		
		if (args.length>=6) {
			MaxRunTimes = Integer.valueOf(args[5]); 
		}
	}
	
	public static void main(String args[]){
		if (args.length < 5) {
			System.out.println("too few arguments.");
			return;
		}
		parseArgs(args);
		
		/** active the specified mutation point */
		mut.Modify.setActivePoint(activeSID);
		
		/** we would run multiple times, each time with a set of different mutations */
		try { 
			for(int i=1;i<=MaxRunTimes;i++){
				System.out.println("====== current at the Run No.  " + i + "======");
				
				/** if we are running multiple times, we do want to use different set of mutations for each time */
				MutOptions.updateSeed();
				
				startRunSubject(i);
			}
		}
		catch (Throwable t) {
			System.setErr(stderr);
			System.err.println("ERROR occurred during the runtime phase!");
			t.printStackTrace(stderr);
		}
		finally {
			System.setErr(stderr);
			System.setOut(stdout);
			System.out.println("Effectively " + (nTests - uncoveredTests.size()) + 
					" Tests have exercised the instrumented programs with the mutation point at " + activeSID);
		}
	}
		
	public static void startRunSubject(int runIdx) {
		String MutEAoutputDir = MutEAOutputPath + File.separator + "/run"+runIdx + File.separator;
		String MutEHoutputDir = MutEHOutputPath + File.separator + "/run"+runIdx + File.separator;
		
		/** we might just need run multiple times from external command line */
		if (MaxRunTimes == 1) {
			MutEAoutputDir = MutEAOutputPath + File.separator;
			MutEHoutputDir = MutEHOutputPath + File.separator;
		}
		
		File dirEAF = new File(MutEAoutputDir);
		if(!dirEAF.isDirectory())	dirEAF.mkdirs();
		File dirEHF = new File(MutEHoutputDir);
		if(!dirEHF.isDirectory())	dirEHF.mkdirs();
		
		int n = 0;
		BufferedReader br;
	
		try {
			br = new BufferedReader(new InputStreamReader(new FileInputStream(testInputFile)));
			String ts = br.readLine();
			while(ts != null && n < nTests){
				n++;
				if (uncoveredTests.contains(n)) {
					/** this test was not covered in the previous run, won't be able to get covered this time, given the same mutation point */
					ts = br.readLine();
					continue;
				}
				String []args = preProcessArg(ts,SubjectDir);
				
				//System.setOut(stdout);
				//System.out.println("test:  " + args[0] + " " + args[1]);
				
				//////////////////////////////// Run MutEA instrumented subject //////////////////////////////////////////////
				System.setOut(stdout);
				System.out.println("For MutEAS current at the test No.  " + n);
				
				String outputEAF = MutEAoutputDir + "test" + n + ".out";
				String errEAF = MutEAoutputDir + "test" + n + ".err";
				
				// redirect stdout and stderr 
				File outputFileEA = new File(outputEAF);
				PrintStream outEA = new PrintStream(new FileOutputStream(outputFileEA)); 
				System.setOut(outEA); 
				File errFileEA = new File(errEAF);
				PrintStream errEA = new PrintStream(new FileOutputStream(errFileEA)); 
				System.setErr(errEA);
				
				// set the name of file as the serialization target of method event maps (F followed by L)
				EAS.Monitor.setEventMapSerializeFile(MutEAoutputDir  + "test"+n+ ".em");
				
				File runSubEA = new File(MutEABinPath);
				@SuppressWarnings("deprecation")
				URL urlEA = runSubEA.toURL();
			    URL[] urlsEA = new URL[]{urlEA};
			    
			    /** Make sure we set the same seed so that for the same test and same mutation point, 
				 * MutEAS and MutEH used the same sequence of random values used for the mutating process
				 */
			    mut.Modify.setSeed(MutOptions.getSeed());
			    /** Also, reset the call counter in the Modifier */
			    mut.Modify.resetCallCounter();
			    
				try {
				    ClassLoader clEA = new URLClassLoader( urlsEA, Thread.currentThread().getContextClassLoader() );
				    Thread.currentThread().setContextClassLoader(clEA);
				    
				    Class<?> clsEA = clEA.loadClass(entryClsName);
				    
				    Method meEA=clsEA.getMethod("main", new Class[]{args.getClass()});

				    meEA.invoke(null, new Object[]{(Object)args});
				}
				catch (Exception e) {
					e.printStackTrace();
				}
				
				// invoke the "program termination event" for the subject in case there is uncaught exception occurred
				EAS.Monitor.terminate("Enforced by EARun.");
				
				outEA.flush();
				outEA.close();
				errEA.flush();
				errEA.close();
				
				boolean hasCoveredForEA = mut.Modify.hasModifiedAny();
				
				//////////////////////////////// Run MutEH instrumented subject //////////////////////////////////////////////
				System.setOut(stdout);
				System.out.println("For MutEH current at the test No.  " + n);
				
				String outputEHF = MutEHoutputDir + "" + n + ".out";
				String errEHF = MutEHoutputDir + "" + n + ".err";
				
				// redirect stdout and stderr 
				File outputFileEH = new File(outputEHF);
				PrintStream outEH = new PrintStream(new FileOutputStream(outputFileEH)); 
				System.setOut(outEH); 
				File errFileEH = new File(errEHF);
				PrintStream errEH = new PrintStream(new FileOutputStream(errFileEH)); 
				System.setErr(errEH);
				
				File runSubEH = new File(MutEHBinPath);
				@SuppressWarnings("deprecation")
				URL urlEH = runSubEH.toURL();        
				URL[] urlsEH = new URL[]{urlEH};
				
				/** Make sure we set the same seed so that for the same test and same mutation point, 
				 * MutEAS and MutEH used the same sequence of random values used for the mutating process
				 */
				mut.Modify.setSeed(MutOptions.getSeed());
				 /** Also, reset the call counter in the Modifier */
			    mut.Modify.resetCallCounter();
			    ExecHistReporter.outStream = outEH;
				try {
					ClassLoader clEH = new URLClassLoader(urlsEH, ClassLoader.getSystemClassLoader());
					Class<?> clsEH = clEH.loadClass(entryClsName);
					
					Method meEH=clsEH.getMethod("main", new Class[]{args.getClass()});
					meEH.invoke(null, new Object[]{(Object)args});
				}
				catch (Exception e) {
					e.printStackTrace();
				}
				
				outEH.flush();
				outEH.close();
				errEH.flush();
				errEH.close();
				
				boolean hasCoveredForEH = mut.Modify.hasModifiedAny();
				/** thing has been wrong if the same mutation point is covered in MutEA but not in MutEH, or the other way around */ 
				assert hasCoveredForEA == hasCoveredForEH;
				
				/** if this test does not cover the specified mutation points, there is no need to run it multiple times*/
				if (!hasCoveredForEH) {
					uncoveredTests.add(n);
				}
	
				ts = br.readLine();
			}
			
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static String[] preProcessArg(String arg,String dir){
		String s1 = arg.replaceAll("\\\\+","/").replaceAll("\\s+", " ");
 
		if(s1.startsWith(" "))
			s1 = s1.substring(1,s1.length());
		String argArray[] =  s1.split(" ");
		for(int i=0;i<argArray.length;i++){
			if(argArray[i].startsWith("..")){
				argArray[i] = argArray[i].replaceFirst("..", dir);
			}
		}		
		return argArray;
	}
} // MutRun

/* vim :set ts=4 tw=4 tws=4 */
