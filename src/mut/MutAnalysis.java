/**
 * File: src/mut/MutAnalysis.java
 * -------------------------------------------------------------------------------------------
 * Date			Author      Changes
 * -------------------------------------------------------------------------------------------
 * 09/26/13		hcai		created; for computing mDEA impact sets as ground truth
 *  
*/
package mut;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class MutAnalysis{
	static Set<String> changeSet = new LinkedHashSet<String>();
	static Map<String, Set<String> > impactSets = new LinkedHashMap<String, Set<String>>();
	static int nExecutions = Integer.MAX_VALUE;
	
	static boolean debugOut = false;
			
	public static void main(String args[]){
		if (args.length < 4) {
			System.err.println("Too few arguments: \n\t " +
					"MutAnalysis changedMethods traceDir binDir [numberTraces] [debugFlag]\n\n");
			return;
		}
		
		String changedMethods = args[0]; // tell the changed methods, separated by comma if there are more than one
		String EHDir1 = args[1]; // tell the directory where one set of execution histories are located
		String EHDir2 = args[2]; // tell the directory where the another set of execution histories are located
		String StmtIdFile = args[3]; // tell the location of the stmtids.out file 
		
		// read at most N execution traces if specified, otherwise exhaust all to be found
		if (args.length > 4) {
			nExecutions = Integer.parseInt(args[4]);
		}
		
		if (args.length > 5) {
			debugOut = args[5].equalsIgnoreCase("-debug");
		}
		
		if (debugOut) {
			System.out.println("Try to read [" + (-1==nExecutions?"All available":nExecutions) + "] EH traces in " 
					+ EHDir1 + " and in " + EHDir2 + " with changed methods being " + changedMethods);
		}
		
		List<String> Chglist = dua.util.Util.parseStringList(changedMethods, ';');
		if (Chglist.size() < 1) {
			// nothing to do
			System.out.println("Empty query, nothing to do.");
			return;
		}
		// determine the valid change set
		Set<String> validChgSet = new LinkedHashSet<String>();
		for (String chg : Chglist) {
			validChgSet.add(chg);
		}
		if (validChgSet.isEmpty()) {
			printStatistics(impactSets, true);
			return;
		}
		changeSet.addAll(validChgSet);
		
		// read stmtIDs file
		deam.DeamAnalysis.readStmtIDsFile(StmtIdFile);

		// differencing execution histories
		Collection<String> result = deam.DeamAnalysis.startDiff(
				EHDir1, EHDir2, args.length>6?args[6] : String.valueOf(Integer.MIN_VALUE), debugOut, nExecutions);
		
		for (String chg : changeSet) {
			impactSets.put(chg, new LinkedHashSet<String>(result));
		}
		
		System.out.println(deam.DeamAnalysis.getNumOfProcessedTests() + " execution traces have been processed.");
		printStatistics(impactSets, true);
	}
		
	private static void printStatistics (Map<String, Set<String>> mis, boolean btitle) {
		if (btitle) {
			System.out.println("\n============ mDEA Result ================");
			System.out.println("[Valid Change Set]");
			for (String m:changeSet) {
				System.out.println(m);
			}
		}
		
		Set<String> aggregatedIS = new LinkedHashSet<String>();
		for (String m : mis.keySet()) {
			if (mis.keySet().size()>1) {
				System.out.println("[Change Impact Set of " + m + "]: size= " + mis.get(m).size());
				for (String im : mis.get(m)) {
					System.out.println(im);
				}
			}
			// merge impact sets of all change queries
			aggregatedIS.addAll(mis.get(m));
		}
		if (btitle) {
			System.out.println("\n[Change Impact Set of All Changes]: size= " + aggregatedIS.size());
			for (String im : aggregatedIS) {
				System.out.println(im);
			}
		}
	}
}

/* vim :set ts=4 tw=4 tws=4 */
