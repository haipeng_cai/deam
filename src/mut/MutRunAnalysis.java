/**
 * File: src/mut/MutRunAnalysis.java
 * -------------------------------------------------------------------------------------------
 * Date			Author      Changes
 * -------------------------------------------------------------------------------------------
 * 09/28/13		hcai		created; for running both MutEAS/MutEH instrumented programs and then 
 *							getting results of EAS and mDEA all in a single process session and all in memory
 *							for 1) easier mechanism of separate analysis for crashing versus non-crashing tests
 *							for each mutation point per method; and 2) saving I/O overhead relative to a standalone
 *							post-processing phase
 * 10/03/13		hcai		added timeout mechanism for running EAS and mDEA with a single test, in order to
 * 							avoid getting stuck on some deadly state caused by certain crazy mutations
 * 12/15/13		hcai		added supports for multi-method change -- assuming that no any two mutation points are to
 *								execute at the same time (be wary of possible runtime mutation chaos with multi-threading subjects)
*/
package mut;

import java.io.*;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import profile.ExecHistReporter;

public class MutRunAnalysis{
	public final static float TRACELEN_THRESHOLD = 0.5f;
	
	private static int MaxRunTimes = 1;
	
	// the necessary location arguments that give the running subjects */
	/** the subject location and version-seed suffix, with which the class paths, inputs and output dirs are to 
	 * be reduced according to the naming routines 
	 */
	protected static String SubjectDir = "";
	protected static String VersionSeed = "";
	/** the entry class name */
	protected static String entryClsName = "";
	/** the query method that encloses the active SID and that is used for querying EAS */
	protected static List<String> queryMethods; // = "";
	/** the mutation location to be activated given by a statement id */
	protected static List<Integer> activeSIDs; // = Integer.MIN_VALUE;
	/** the file giving the EAS full trace length per test - we will these length to determine if a mutation crashed a test */
	protected static String fnTraceLengths = "";
	/** the file giving stmtids.out of the Mutation-instrumented subject */
	protected static String StmtIdFile = "";
		
	// the following parameters are to be reduced from the above according to the naming routines */
	/** the class path for the MutEA instrumented subject */
	protected static String MutEABinPath="";
	/** the output path for the MutEA instrumented subject */
	protected static String MutEAOutputPath="";
	/** the class path for the MutEH instrumented subject */
	protected static String MutEHBinPath="";
	/** the output path for the MutEA instrumented subject */
	protected static String MutEHOutputPath="";
	/** the output path for the NonMutEA instrumented subject */
	protected static String NonMutEHOutputPath="";
	
	/** the input text that indexes all test cases */
	protected static String testInputFile ="";
	/** the number of tests to execute */
	protected static int	nTests = 0;
	
	/** keep the default output stream */
	private final static PrintStream stdout = System.out;
	private final static PrintStream stderr = System.err;
	
	/** maintain a list of uncovered tests to save run times */
	protected static Set<Integer> uncoveredTests = new LinkedHashSet<Integer>();
	
	/** map from test to corresponding expected full EAS trace length */
	protected static Map<String, Integer> test2tracelen = new LinkedHashMap<String, Integer>();
	
	/** total crashing tests */
	protected static Integer totalCrashingTests = 0;
	
	/** total number of skipped tests found not covering the query method according EAS full trace */
	protected static Integer totalSkippedTests = 0;
	
	/** in order to avoid crashing the disk space, remove outputs after they have been utilized for IS computations */
	protected static boolean keepOutputs = false;
	
	/** to avoid getting stuck on some test execution because of certain crazy mutations, 
	 * we limit the maximal length of time for each, in minutes */
	protected static long MAXTESTDURATION = 30L;
	
	/** the map from category to impact set: 
	 * 0: EAS-all 
	 * 1: EAS-crashing 
	 * 2: EAS-non-crashing 
	 * 3: mDEA-all 
	 * 4: mDEA-crashing 
	 * 5: mDEA-non-crashing 
	 */
	protected static Map<Integer, Set<String>> finalResult = new LinkedHashMap<Integer, Set<String>>();
	
	public static void parseArgs(String args[]){
		assert args.length >= 8;
		SubjectDir = args[0];
		VersionSeed = args[1];
		entryClsName = args[2];
		/*
		queryMethod = args[3];
		activeSID = Integer.valueOf(args[4]);
		*/
		queryMethods = dua.util.Util.parseStringList(args[3], ';');
		activeSIDs = dua.util.Util.parseIntList(args[4]);
		
		fnTraceLengths = args[5];
		StmtIdFile = args[6];
		nTests = Integer.valueOf(args[7]);
		System.err.println("Subject: " + SubjectDir + " ver-seed=" + VersionSeed +
				" boostrap class=" + entryClsName + " query methods=" + queryMethods + 
				" activeSIDs=" + activeSIDs + " number of tests=" + nTests);
		
		MutEABinPath = SubjectDir + File.separator + "MutEAInstrumented-" + VersionSeed;
		MutEAOutputPath = SubjectDir + File.separator + "MutEAoutdyn-" + VersionSeed;
		MutEHBinPath = SubjectDir + File.separator + "MutEHInstrumented-" + VersionSeed;
		MutEHOutputPath = SubjectDir + File.separator + "MutEHoutdyn-" + VersionSeed;
		
		NonMutEHOutputPath = SubjectDir + File.separator + "NonMutEHoutdyn-" + VersionSeed;
		
		testInputFile = SubjectDir + File.separator + "inputs" + File.separator + "testinputs.txt";
		
		if (args.length>=9) {
			int MaxInvocation = Integer.valueOf(args[8]);
			Modify.MAXINVOCATION = MaxInvocation;
		}
		if (args.length>=10) {
			long MaxTestDuration = Long.valueOf(args[9]);
			MAXTESTDURATION = MaxTestDuration;
		}
		
		if (args.length>=11) {
			keepOutputs = args[10].equalsIgnoreCase("-keepOutputs");
		}
		if (args.length>=12) {
			MaxRunTimes = Integer.valueOf(args[11]); 
		}
	}
	
	public static void readTraceLengths() {
		BufferedReader br;
	
		try {
			br = new BufferedReader(new InputStreamReader(new FileInputStream(fnTraceLengths)));
			String ts = br.readLine();
			while(ts != null){
				String[] segs = ts.split("\\s+",2);
				assert segs.length >=2;
				test2tracelen.put(segs[0], Integer.valueOf(segs[1]));
				
				ts = br.readLine();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		if (test2tracelen.keySet().size() != nTests) {
			System.setErr(stderr);
			System.err.println("WARNING: the given test case number [" + nTests +
					"] does NOT match the number of tests from the trace length file " + test2tracelen.keySet().size());
		}
	}
	
	public static void main(String args[]){
		if (args.length < 5) {
			System.err.println("too few arguments.");
			return;
		}
		parseArgs(args);
		
		readTraceLengths();
		
		// read stmtIDs file
		deam.DeamAnalysis.readStmtIDsFile(StmtIdFile);
		
		/** active the specified mutation point */
		mut.Modify.setActivePoints(activeSIDs);
		
		/** we would run multiple times, each time with a set of different mutations */
		for(int i=1;i<=MaxRunTimes;i++){
			System.out.println("====== current at the Run No.  " + i + "======");
			try {
				/** if we are running multiple times, we do want to use different set of mutations for each time */
				MutOptions.updateSeed();
				totalSkippedTests = 0;
				totalCrashingTests = 0;
				
				startRunSubject(i);
			}
			catch (Throwable t) {
				//System.setErr(stderr);
				System.err.println("ERROR occurred during the runtime phase!");
				t.printStackTrace(stderr);
				/** once fatal errors occurred, won't continue the next run */
			}
			finally {
				//System.setErr(stderr);
				//System.setOut(stdout);
				System.out.println("Effectively " + (nTests - uncoveredTests.size()) + 
						" Tests have exercised the instrumented programs with the mutation points at " + activeSIDs);
			}
		}
	}
		
	public static void startRunSubject(int runIdx) {
		String MutEAoutputDir = MutEAOutputPath + File.separator + "/run"+runIdx + File.separator;
		String MutEHoutputDir = MutEHOutputPath + File.separator + "/run"+runIdx + File.separator;
		
		/** we might just need run multiple times from external command line */
		if (MaxRunTimes == 1) {
			MutEAoutputDir = MutEAOutputPath + File.separator;
			MutEHoutputDir = MutEHOutputPath + File.separator;
		}
		
		File dirEAF = new File(MutEAoutputDir);
		if(!dirEAF.isDirectory())	dirEAF.mkdirs();
		File dirEHF = new File(MutEHoutputDir);
		if(!dirEHF.isDirectory())	dirEHF.mkdirs();
		
		int n = 0;
		BufferedReader br;
		
		// initialize the final result map
		for (Integer cat = 0; cat < 6; cat++) {
			finalResult.put(cat, new LinkedHashSet<String>());
		}
		
		List<String> Chglist = new ArrayList<String>();
		//Chglist.add(queryMethod);
		Chglist.addAll(queryMethods);
	
		try {
			br = new BufferedReader(new InputStreamReader(new FileInputStream(testInputFile)));
			String ts = br.readLine();
			while(ts != null && ts.trim().length()>1 && n < nTests){
				n++;
				if (uncoveredTests.contains(n)) {
					/** this test was not covered in the previous run, won't be able to get covered this time, given the same mutation point */
					ts = br.readLine();
					continue;
				}
				final String []args = preProcessArg(ts,SubjectDir);
				
				//System.setOut(stdout);
				//System.out.println("test:  " + args[0] + " " + args[1]);
				
				//////////////////////////////// Run MutEA instrumented subject //////////////////////////////////////////////
				System.setErr(stderr);
				System.err.println("For MutEAS current at the test No.  " + n);
				
				final String outputEAF = MutEAoutputDir + "test" + n + ".out";
				final String errEAF = MutEAoutputDir + "test" + n + ".err";
				
				// redirect stdout and stderr 
				final File outputFileEA = new File(outputEAF);
				final PrintStream outEA = new PrintStream(new FileOutputStream(outputFileEA)); 
				System.setOut(outEA); 
				final File errFileEA = new File(errEAF);
				final PrintStream errEA = new PrintStream(new FileOutputStream(errFileEA)); 
				System.setErr(errEA);
				
				// set the name of file as the serialization target of method event maps (F followed by L)
				EAS.Monitor.setEventMapSerializeFile(MutEAoutputDir  + "test"+n+ ".em");
				
				final File runSubEA = new File(MutEABinPath);
				@SuppressWarnings("deprecation")
				final URL urlEA = runSubEA.toURL();
			    final URL[] urlsEA = new URL[]{urlEA};
			    
			    /** Make sure we set the same seed so that for the same test and same mutation point, 
				 * MutEAS and MutEH used the same sequence of random values used for the mutating process
				 */
			    mut.Modify.setSeed(MutOptions.getSeed());
			    /** Also, reset the call counter in the Modifier */
			    mut.Modify.resetCallCounter();
			    
			    ExecutorService EAservice = Executors.newSingleThreadExecutor();
			    try {
			        Runnable EARunner = new Runnable() {
				@Override public void run() {
				
				try {
				    ClassLoader clEA = new URLClassLoader( urlsEA, Thread.currentThread().getContextClassLoader() );
				    Thread.currentThread().setContextClassLoader(clEA);
				    
				    Class<?> clsEA = clEA.loadClass(entryClsName);
				    
				    Method meEA=clsEA.getMethod("main", new Class[]{args.getClass()});

				    meEA.invoke(null, new Object[]{(Object)args});
				}
				catch (Exception e) {
					e.printStackTrace();
				}
				
				outEA.flush();
				outEA.close();
				errEA.flush();
				errEA.close();
				
				}};
				Future<?>  EAfuture = EAservice.submit(EARunner);
				EAfuture.get(MAXTESTDURATION*60, TimeUnit.SECONDS);
			    }
			    catch (final InterruptedException e) {
			        // The thread was interrupted during sleep, wait or join
			    	System.setErr(stderr);
					System.err.println("Running MutEAS at the test No.  " + n + " thread interrupted.");
					EAS.Monitor.terminate("Enforced by EARun.");
					ts = br.readLine();
					EAservice.shutdown();
					continue;
			    }
			    catch (final TimeoutException e) {
			        // Took too long!
			    	System.setErr(stderr);
					System.err.println("Running MutEAS at the test No.  " + n + " TimeOut after " + MAXTESTDURATION*60 + " seconds");
					EAS.Monitor.terminate("Enforced by EARun.");
					ts = br.readLine();
					EAservice.shutdown();
					continue;
			    }
			    catch (final ExecutionException e) {
			        // An exception from within the Runnable task
			    	System.setErr(stderr);
					System.err.println("Running MutEAS at the test No.  " + n + " exception thrown during test execution " + e);
					EAS.Monitor.terminate("Enforced by EARun.");
					//System.exit(0);
					ts = br.readLine();
					EAservice.shutdown();
					continue;
			    }
			    finally {
			        EAservice.shutdown();
			        outEA.flush();
					outEA.close();
					errEA.flush();
					errEA.close();
			    }
			    
			    // invoke the "program termination event" for the subject in case there is uncaught exception occurred
			    EAS.Monitor.terminate("Enforced by EARun.");
				
				/** determine whether the current test is a crashing one simply according to the full EAS trace length, compared to the
				 * expected one (obtained from the fixed version)
				 */
				assert test2tracelen.get("test"+n)!=null;
				boolean isCrashing = (EAS.Monitor.getFullTraceLength()*1.0 / test2tracelen.get("test"+n) <= TRACELEN_THRESHOLD);
				
				boolean hasCoveredForEA = mut.Modify.hasModifiedAny();
				//////////////////////////////// Compute EAS impact set //////////////////////////////////////////////
				System.setErr(stderr);
				System.err.println("Computing EAS impact set ......  ");
				Set<String> EAIS = new LinkedHashSet<String>();
				int szEAIS = EAS.EAAnalysis.parseSingleTrace(MutEAoutputDir, n, Chglist, EAIS);
				if (!keepOutputs) {
					deleteFile(outputEAF);
					deleteFile(errEAF);
				}
				if (szEAIS<1) {
					// this test did not cover the query method, no point to run with MutEH because the mutation point is within the method
					System.err.println("Non-covering test skipped ......  ");
					totalSkippedTests ++;
					ts = br.readLine();
					continue;
				}
				
				//////////////////////////////// Run MutEH instrumented subject //////////////////////////////////////////////
				System.setErr(stderr);
				System.err.println("For MutEH current at the test No.  " + n);
				
				final String outputEHF = MutEHoutputDir + "" + n + ".out";
				final String errEHF = MutEHoutputDir + "" + n + ".err";
				
				// redirect stdout and stderr 
				final File outputFileEH = new File(outputEHF);
				final PrintStream outEH = new PrintStream(new FileOutputStream(outputFileEH)); 
				//System.setOut(outEH);
				ExecHistReporter.outStream = outEH;
				final File errFileEH = new File(errEHF);
				final PrintStream errEH = new PrintStream(new FileOutputStream(errFileEH)); 
				System.setErr(errEH);
				
				final File runSubEH = new File(MutEHBinPath);
				@SuppressWarnings("deprecation")
				final URL urlEH = runSubEH.toURL();        
				final URL[] urlsEH = new URL[]{urlEH};
				
				/** Make sure we set the same seed so that for the same test and same mutation point, 
				 * MutEAS and MutEH used the same sequence of random values used for the mutating process
				 */
				mut.Modify.setSeed(MutOptions.getSeed());
				 /** Also, reset the call counter in the Modifier */
			    mut.Modify.resetCallCounter();
			    
			    ExecutorService EHservice = Executors.newSingleThreadExecutor();
			    try {
			        Runnable EHRunner = new Runnable() {
					@Override public void run() {
									
					try {
						/*
						ClassLoader clEH = new URLClassLoader(urlsEH, ClassLoader.getSystemClassLoader());
						Class<?> clsEH = clEH.loadClass(entryClsName);
						
						Method meEH=clsEH.getMethod("main", new Class[]{args.getClass()});
						meEH.invoke(null, new Object[]{(Object)args});
						*/
						
						ClassLoader clEH = new URLClassLoader( urlsEH, Thread.currentThread().getContextClassLoader() );
						Thread.currentThread().setContextClassLoader(clEH);
						    
					    Class<?> clsEH = clEH.loadClass(entryClsName);
					    
					    Method meEH=clsEH.getMethod("main", new Class[]{args.getClass()});
	
					    meEH.invoke(null, new Object[]{(Object)args});
					}
					catch (Exception e) {
						e.printStackTrace();
					}
					
					outEH.flush();
					outEH.close();
					errEH.flush();
					errEH.close();
					
					}};
					Future<?>  EHfuture = EHservice.submit(EHRunner);
					EHfuture.get(MAXTESTDURATION*60*2, TimeUnit.SECONDS); // mDEA is expected to take longer to run than EAS
			    }
			    catch (final InterruptedException e) {
			        // The thread was interrupted during sleep, wait or join
			    	System.setErr(stderr);
					System.err.println("Running MutEH at the test No.  " + n + " thread interrupted.");
					ts = br.readLine();
					EHservice.shutdown();
					continue;
			    }
			    catch (final TimeoutException e) {
			        // Took too long!
			    	System.setErr(stderr);
					System.err.println("Running MutEH at the test No.  " + n + " TimeOut after " + MAXTESTDURATION*60*2 + " seconds");
					ts = br.readLine();
					EHservice.shutdown();
					continue;
			    }
			    catch (final ExecutionException e) {
			        // An exception from within the Runnable task
			    	System.setErr(stderr);
					System.err.println("Running MutEH at the test No.  " + n + " exception thrown during test execution " + e);
					System.exit(0);
					ts = br.readLine();
					EHservice.shutdown();
					continue;
			    }
			    finally {
			        EHservice.shutdown();
			        outEH.flush();
					outEH.close();
					errEH.flush();
					errEH.close();
			    }
				
				boolean hasCoveredForEH = mut.Modify.hasModifiedAny();
				//////////////////////////////// Compute mDEA impact set //////////////////////////////////////////////
				System.setErr(stderr);
				System.err.println("Computing mDEA impact set ......  ");
				File outputFileNonMutEHF = new File(NonMutEHOutputPath + File.separator + "" + n + ".out");
				System.setErr(stderr);
				Collection<String> __mDEAIS = deam.DeamAnalysis.diffExecutionHistory(outputFileEH, 
						outputFileNonMutEHF, String.valueOf(Integer.MIN_VALUE));
				if (!keepOutputs) {
					deleteFile(outputEHF);
					deleteFile(errEHF);
				}
				Set<String> mDEAIS = new LinkedHashSet<String>(__mDEAIS);
								
				/** thing has been wrong if the same mutation point is covered in MutEA but not in MutEH, or the other way around */ 
				//assert hasCoveredForEA == hasCoveredForEH;
				if (hasCoveredForEA != hasCoveredForEH) {
					ts = br.readLine();
					continue;
				}
				
				/** if this test does not cover the specified mutation points, there is no need to run it multiple times*/
				if (!hasCoveredForEH) {
					uncoveredTests.add(n);
				}
				
				//////////////////////////////// merge result  //////////////////////////////////////////////
				finalResult.get(0).addAll(EAIS);
				finalResult.get(3).addAll(mDEAIS);
				
				finalResult.get(isCrashing?1:2).addAll(EAIS);
				finalResult.get(isCrashing?4:5).addAll(mDEAIS);
				
				if (isCrashing) { 	totalCrashingTests ++; }

				// next test case
				ts = br.readLine();
			}
			
			br.close();
		} catch (Exception e) {
			System.setErr(stderr);
			e.printStackTrace();
			//return;
		}
		finally {
			dumpStatistics(stdout,stderr);
			ExecHistReporter.outStream = null;
		}
	}
	
	/** dump impact sets of EAS versus mDEA for the specific mutation point in the given query method */ 
	public static void dumpStatistics(PrintStream os, PrintStream or) {
		if (finalResult.get(0).size() < 1) {
			// if this query method is not covered by any of the nTests tests, nothing to be reported
			return;
		}
		
		// dump complete data
		or.println("==== All EAS impact set of [" + queryMethods +"] at location [" + activeSIDs + "] ====");
		for (String m : finalResult.get(0)) {
			or.println(m);
		}
		or.println("==== Crashing EAS impact set of [" + queryMethods +"] at location [" + activeSIDs + "] ====");
		for (String m : finalResult.get(1)) {
			or.println(m);
		}
		or.println("==== Non-crashing EAS impact set of [" + queryMethods +"] at location [" + activeSIDs + "] ====");
		for (String m : finalResult.get(2)) {
			or.println(m);
		}
		
		or.println("==== All mDEA impact set of [" + queryMethods +"] at location [" + activeSIDs + "] ====");
		for (String m : finalResult.get(3)) {
			or.println(m);
		}
		or.println("==== Crashing mDEA impact set of [" + queryMethods +"] at location [" + activeSIDs + "] ====");
		for (String m : finalResult.get(4)) {
			or.println(m);
		}
		or.println("==== Non-crashing mDEA impact set of [" + queryMethods +"] at location [" + activeSIDs + "] ====");
		for (String m : finalResult.get(5)) {
			or.println(m);
		}
		
		// compute FPs and FNs
		Set<String> IntersectionAll = new LinkedHashSet<String>(finalResult.get(0)); 
		IntersectionAll.retainAll(finalResult.get(3));
		Set<String> FPAll = new LinkedHashSet<String>(finalResult.get(0));
		FPAll.removeAll(finalResult.get(3));
		Set<String> FNAll = new LinkedHashSet<String>(finalResult.get(3));
		FNAll.removeAll(finalResult.get(0));
		
		Set<String> IntersectionC = new LinkedHashSet<String>(finalResult.get(1)); 
		IntersectionC.retainAll(finalResult.get(4));
		Set<String> FPC = new LinkedHashSet<String>(finalResult.get(1));
		FPC.removeAll(finalResult.get(4));
		Set<String> FNC = new LinkedHashSet<String>(finalResult.get(4));
		FNC.removeAll(finalResult.get(1));
		
		Set<String> IntersectionNC = new LinkedHashSet<String>(finalResult.get(2)); 
		IntersectionNC.retainAll(finalResult.get(5));
		Set<String> FPNC = new LinkedHashSet<String>(finalResult.get(2));
		FPNC.removeAll(finalResult.get(5));
		Set<String> FNNC = new LinkedHashSet<String>(finalResult.get(5));
		FNNC.removeAll(finalResult.get(2));
		
		or.println("==== All EAS False-positives of [" + queryMethods +"] at location [" + activeSIDs + "] ====");
		for (String m : FPAll) {
			or.println(m);
		}
		or.println("==== All EAS False-negatives of [" + queryMethods +"] at location [" + activeSIDs + "] ====");
		for (String m : FNAll) {
			or.println(m);
		}
		or.println("==== Crashing EAS False-positives of [" + queryMethods +"] at location [" + activeSIDs + "] ====");
		for (String m : FPC) {
			or.println(m);
		}
		or.println("==== Crashing EAS False-negatives of [" + queryMethods +"] at location [" + activeSIDs + "] ====");
		for (String m : FNC) {
			or.println(m);
		}
		or.println("==== Non-crashing EAS False-positives of [" + queryMethods +"] at location [" + activeSIDs + "] ====");
		for (String m : FPNC) {
			or.println(m);
		}
		or.println("==== Non-crashing EAS False-negatives of [" + queryMethods +"] at location [" + activeSIDs + "] ====");
		for (String m : FNNC) {
			or.println(m);
		}
		or.println();
		or.flush();
		
		DecimalFormat df = new DecimalFormat("#.####");
		
		os.println("==== crashing versus non-crashing tests [" + totalCrashingTests +" : " + (nTests - totalCrashingTests) + "] ====");
		os.println("==== valid versus non-covering tests [" + (nTests-totalSkippedTests) +" : " + totalSkippedTests + "] ====");
		// compute statistics and dump
		//String title1 = "change\tquery\tmp\tEA\tmDEA\tFP\tFN\tprecision\trecall\tF1\tF2";
		String title = VersionSeed + "\t" + queryMethods + "\t" + activeSIDs + "\t";
		
		double pAll = finalResult.get(0).size()<1?1.0:IntersectionAll.size()*1.0 / finalResult.get(0).size();
		double cAll = finalResult.get(3).size()<1?1.0:IntersectionAll.size()*1.0 / finalResult.get(3).size();
		double pC = finalResult.get(1).size()<1?1.0:IntersectionC.size()*1.0 / finalResult.get(1).size();
		double cC = finalResult.get(4).size()<1?1.0:IntersectionC.size()*1.0 / finalResult.get(4).size();
		double pNC = finalResult.get(2).size()<1?1.0:IntersectionNC.size()*1.0 / finalResult.get(2).size();
		double cNC = finalResult.get(5).size()<1?1.0:IntersectionNC.size()*1.0 / finalResult.get(5).size();
		
		os.print("[All] " + title);
		os.print(finalResult.get(0).size() + "\t" + finalResult.get(3).size() + "\t" + FPAll.size() + "\t" +
				FNAll.size() + "\t" + df.format(pAll) + "\t" + df.format(cAll) + "\t" + 
				df.format(FMeasure(pAll, cAll, 1)) + "\t" + df.format(FMeasure(pAll, cAll, 2)) + "\n");
		
		os.print("[C] " + title);
		os.print(finalResult.get(1).size() + "\t" + finalResult.get(4).size() + "\t" + FPC.size() + "\t" +
				FNC.size() + "\t" + df.format(pC) + "\t" + df.format(cC) + "\t" + 
				df.format(FMeasure(pC, cC, 1)) + "\t" + df.format(FMeasure(pC, cC, 2)) + "\n");
		
		os.print("[NC] " + title);
		os.print(finalResult.get(2).size() + "\t" + finalResult.get(5).size() + "\t" + FPNC.size() + "\t" +
				FNNC.size() + "\t" + df.format(pNC) + "\t" + df.format(cNC) + "\t" + 
				df.format(FMeasure(pNC, cNC, 1)) + "\t" + df.format(FMeasure(pNC, cNC, 2)) + "\n");
		os.println();
		os.flush();
	}
	
	public static double FMeasure(double p, double c, int b) {
		return ( (1+b*b)*(p*c)/(p*(b*b)+c) );
	}
	
	public static String[] preProcessArg(String arg,String dir){
		String s1 = arg.replaceAll("\\\\+","/").replaceAll("\\s+", " ");
 
		if(s1.startsWith(" "))
			s1 = s1.substring(1,s1.length());
		String argArray[] =  s1.split(" ");
		for(int i=0;i<argArray.length;i++){
			if(argArray[i].startsWith("..")){
				argArray[i] = argArray[i].replaceFirst("..", dir);
			}
		}		
		return argArray;
	}
	
	public static int deleteFile(String fname) {
		File fObj = new File(fname);
		try {
			if (fObj.exists() && fObj.isFile()) {
				fObj.delete();
			}
		} 
		catch (SecurityException e) { System.err.println("Couldn't delete file due to security reasons: " + fObj + e); return -2;}
		catch (Throwable e) { System.err.println("Couldn't delete file due to unexpected reason: " + fObj + e); return -1;}
		return 0;
	}
	
	public static int countLines(File aFile) throws IOException {
	    LineNumberReader reader = null;
	    try {
	        reader = new LineNumberReader(new FileReader(aFile));
	        while ((reader.readLine()) != null);
	        return reader.getLineNumber();
	    } catch (Exception ex) {
	        return -1;
	    } finally {
	        if(reader != null) 
	            reader.close();
	    }
	}
} // MutRunAnalysis

/* vim :set ts=4 tw=4 tws=4 */
