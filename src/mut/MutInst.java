/**
 * File: src/mut/MutInst.java
 * -------------------------------------------------------------------------------------------
 * Date			Author      Changes
 * -------------------------------------------------------------------------------------------
 * 09/24/13		hcai		created; for instrumenting full-program Mutations and EAS/EH probes 
 * 09/25/13		hcai		fixed Mutation+EH mixed instrumentation issues
 * 01/11/14		hcai		added option of instrumenting all applicable mutation points instead of randomly selecting a subset
*/
package mut;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import profile.ExecHistInstrumenter;

import dua.Forensics;
import fault.StmtMapper;

import soot.*;
import soot.jimple.AnyNewExpr;
import soot.jimple.AssignStmt;
import soot.jimple.ConditionExpr;
import soot.jimple.Constant;
import soot.jimple.IfStmt;
import soot.jimple.Stmt;
import EAS.*;
import MciaUtil.utils;
import Sensa.SensaInst;

public class MutInst extends EAInst {
	protected static MutOptions opts = new MutOptions();

	/** the set of methods having at least some applicable points for mutation instrumentation */
	protected Set<SootMethod> applicableMethods = new LinkedHashSet<SootMethod>();
	/** the set of all applicable points for mutation instrumentation */
	protected Set<Stmt> applicablePnts = new LinkedHashSet<Stmt>();
	/** the eventually used points for mutation instrumentation */
	protected Set<Stmt> selectedPnts = new LinkedHashSet<Stmt>();
	
	/** map: method to selected mutation points in it */
	protected Map<SootMethod, List<Stmt>> me2selpnts = new LinkedHashMap<SootMethod, List<Stmt>>();
	
	/** total number of statements in the subject */
	protected int nStmtTotal = 0;
	/** total number of statements of all mutation-applicable methods */
	protected int nStmtCovered = 0;
	
	public static void main(String args[]){
		args = preProcessArgs(opts, args);
		
		MutInst eaInst = new MutInst();
				
		Forensics.registerExtension(eaInst);
		Forensics.main(args);
	}
	
	@Override public void run() {
		System.out.println("Running MutEAS extension of DUA-Forensics");
		StmtMapper.getCreateInverseMap();
		
		// 1. collect all points to instrument mutations
		collectMutationPoints();
		
		// 2. insert mutation probes at all selected points
		instrAllMutations(selectedPnts);
		System.out.println(selectedPnts.size() + " points have been instrumented with mutations, covering "
				+ applicableMethods.size() + " methods.");
		
		dumpSelectedMutPoints();
		
		System.out.println("The percentage of statements covered by mutations = " + nStmtCovered + "/" + nStmtTotal + "=" 
				+ nStmtCovered*1.0/nStmtTotal*100 + "%");
		
		// 3. either instrument EH probes or EAS events
		if (opts.instrExecHist()) {
			/** important order: if we are doing Mutation+EH, both at full-program level, we 
			 *  should do the EH instrumentation After having inserted Mutation probes, as such
			 *  the EH probe will print the mutated values instead of the old/original values, which is the correct way
			 *  
			 *  However, this seems not feasible to do in a single Soot/DuaF analysis process because there is no support
			 *  to update the analysis information to cover those mutation probes added before  
			 */
			String[] blacklist = new String[opts.blacktypes().size()];
			opts.blacktypes().toArray(blacklist);
			ExecHistInstrumenter instr = new ExecHistInstrumenter(blacklist);
			System.out.println("ExecHistory Instrumentation at everywhere...");
			instr.instrument(true);
			return;
		}
		
		if (opts.instrEAS()) {
			// Cover catch blocks when doing EAS instrumentation
			dua.Options.ignoreCatchBlocks = false;
			this.instrument();
		}
	}
	
	private int collectMutationPoints()	{
		final RefType strType = Scene.v().getRefType("java.lang.String");
			
		/* traverse all classes */
		Iterator<SootClass> clsIt = Scene.v().getClasses().iterator();
		while (clsIt.hasNext()) {
			SootClass sClass = (SootClass) clsIt.next();
			if ( sClass.isPhantom() ) {
				// skip phantom classes
				continue;
			}
			if ( !sClass.isApplicationClass() ) {
				// skip library classes
				continue;
			}
			
			/* traverse all methods of the class */
			Iterator<SootMethod> meIt = sClass.getMethods().iterator();
			while (meIt.hasNext()) {
				SootMethod sMethod = (SootMethod) meIt.next();
				if ( !sMethod.isConcrete() ) {
					// skip abstract methods and phantom methods, and native methods as well
					continue; 
				}
				if ( sMethod.toString().indexOf(": java.lang.Class class$") != -1 ) {
					// don't handle reflections now either
					continue;
				}
				
				// cannot instrument method event for a method without active body
				if ( !sMethod.hasActiveBody() ) {
					continue;
				}
				
				Set<Stmt> localApplicablePnts = new LinkedHashSet<Stmt>();
				
				int localNumberStmts = 0;
				
				Body b = sMethod.retrieveActiveBody();
				Iterator<Unit> iter = b.getUnits().iterator();
				while (iter.hasNext()) {
					Stmt s = (Stmt)iter.next();
					
					if (utils.getFlexibleStmtId(s)==-1) {
						// skip catch blocks or other constructs that DuaF does not handle for now
						continue;
					}
					localNumberStmts ++;
					
					if ( !(s instanceof AssignStmt || s instanceof IfStmt)  ) {
						// SensA supports instrumenting mutations for assignments and branch conditions only for now 
						continue;
					}
					Value targetVal = null;
					if (s instanceof AssignStmt) {
						targetVal = ((AssignStmt)s).getLeftOp();
						if (!(targetVal instanceof Local)) {
							// SensA supports mutating Local values only for now
							continue;
						}
						Value rv = ((AssignStmt)s).getRightOp();
						if ( (rv instanceof AnyNewExpr) /*|| (rv instanceof Constant)*/) {
							// object instantiation is not really a value assignment in fact
							continue;
						}
					}
					else {
						ConditionExpr condExp = (ConditionExpr)((IfStmt) s).getCondition();
						targetVal = condExp.getOp2();
						if (!(targetVal instanceof Local)) {
							// SensA supports mutating Local values only for now
							targetVal = condExp.getOp1();
						}
						if (!(targetVal instanceof Local)) {
							// both sides are not Local value, then SensA can not work with it
							continue;
						}
					}
					
					if ( !(targetVal.getType() instanceof soot.PrimType) && !targetVal.getType().equals(strType) )  {
						// if consider all applicable points, include points where the target value has a current value of "null"
						if (opts.allApplicablePoints()) {
							if (s instanceof AssignStmt) {
								continue;
								/*
								if ( !( ((AssignStmt)s).getRightOp().getType() instanceof soot.NullType ) ) {
									continue;
								}
								*/
							}
							else {
								ConditionExpr condExp = (ConditionExpr)((IfStmt) s).getCondition();
								if (!( condExp.getOp1().getType() instanceof soot.NullType ) && !( condExp.getOp2().getType() instanceof soot.NullType ) ) {
									continue;
								}
							}
						} else {
							// SensA supports mutating Local values of primitive types or String type only for now
							continue;
						}
					}
					
					// gather applicable mutation points in current method
					localApplicablePnts.add(s);
				}
				
				nStmtTotal += localNumberStmts;
				
				if (localApplicablePnts.size()<1) {
					// the current method is not applicable for mutations since it has no any applicable mutation points
					continue;
				}
				
				nStmtCovered += localNumberStmts;

				// gather all applicable mutation points
				applicablePnts.addAll(localApplicablePnts);
				
				// also, record all applicable methods
				applicableMethods.add(sMethod);
				
				// determine which applicable points to be used for actually instrumenting mutants
				List<Stmt> selpnts;
				if (opts.allApplicablePoints()) {
					selpnts = new LinkedList<Stmt>(localApplicablePnts);
					selectedPnts.addAll(localApplicablePnts);
				}
				else {
					selpnts = selectMutationPoints(new LinkedList<Stmt>(localApplicablePnts));
				}
				me2selpnts.put(sMethod, selpnts);
			}
		}
		return 0;
	}
	
	protected List<Stmt> selectMutationPoints(List<Stmt> applicablePnts)	{
		int applicableNum = applicablePnts.size();
		List<Stmt> selpnts = new LinkedList<Stmt>();
		// select all if there are too few (<=5) applicable points 
		if (applicableNum <= 5) {
			selectedPnts.addAll(applicablePnts);
			selpnts.addAll(applicablePnts);
			return selpnts;
		}
		
		int targetNum = Math.min(10, (int) Math.max(5, Math.ceil(applicableNum*0.1)));
		int step = (int)Math.floor(applicableNum*1.0/targetNum);
		
		// randomly select a point for every step points
		Random r = new Random(MutOptions.getFixedSeed()/*System.currentTimeMillis()*/);
		for (int i = 0; i < applicableNum; i+=step) {
			int idx = i + r.nextInt(step);
			idx = Math.min(idx, applicableNum-1);
			selpnts.add(applicablePnts.get(idx));
		}
		selectedPnts.addAll(selpnts);
		return selpnts;
	}
	
	protected int instrAllMutations(Set<Stmt> mutPnts) {
		SensaInst.init("mut.Modify");
		//SensaInst.modifyCla = Scene.v().getSootClass("mut.Modify");
		//SensaInst.modifyMet = SensaInst.modifyCla.getMethodByName("modify");
		for (Stmt s : mutPnts) {
			SensaInst.instrModification(s, true);
		}
		return 0;
	}
	
	protected void dumpSelectedMutPoints() {
		// prepare file to which selected mutation locations will be dumped
		File fnMutLocs = new File(dua.util.Util.getCreateBaseOutPath() + "MutPoints.out");
		try {
			BufferedWriter mlocWriter = new BufferedWriter(new FileWriter(fnMutLocs));
			
			for (SootMethod m : me2selpnts.keySet()) {
					String line = m.getSignature();
					for (Stmt s : me2selpnts.get(m)) {
						line += " " + StmtMapper.getGlobalStmtId(s);
					}
					
					mlocWriter.write(line+"\n");
			}
			mlocWriter.flush();
			mlocWriter.close();
		}
		catch (FileNotFoundException e) { System.err.println("Couldn't write Jimple file: " + fnMutLocs + e); }
		catch (SecurityException e) { System.err.println("Couldn't write Jimple file: " + fnMutLocs + e); }
		catch (IOException e) { System.err.println("Couldn't write Jimple file: " + fnMutLocs + e); }
	}
	
} // -- public class MutInst  

/* vim :set ts=4 tw=4 tws=4 */

